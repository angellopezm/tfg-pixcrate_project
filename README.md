# PIXCRATE #

### What is this repository for? ###

* This repository belongs to Angel Lopez. Pixcrate intends to be a tool for creatives and artists, teams or lone wolves who want to grow as artist and reach more public.
#### Last Version: Beta 1
### How do I get set up? ###

1. CLONE REPOSITORY
2. EDIT DATABASE PARAMETERS IN 'config.php' FROM '/core' FOLDER
3. TEST WITH XAMPP, WAMPP...etc

### Who do I talk to? ###

* Angel Lopez alm.angel97@gmail.com

## **NOTA PREVIA**: EL DED Y EL ERD ESTÁN EN LA CARPETA /library/img dentro de la carpeta PIXCRATE

# PRESENTACIÓN

Antes de todo, me gustaría decir que soy totalmente consciente de las carencias de Pixcrate en esta beta. Código obsoleto, repetición de bloques de código, estilo pésimo (en la zona de administración), etc...

Dicho esto, he aquí la explicación:

- Código obsoleto: Sólo hay una función obsoleta. Me di cuenta de ello hace una semana, cuando entregué el proyecto de javascript. Debido a esto cási no lo entrego, ya que daba fallas en otros equipos.

- Repetición de bloques de código: Sobre esto debo decir que, debido a que el tiempo se ajustaba cada vez más y más y las fechas de entrega se aproximaban he llevado arrastrando este problema de hace un par de semanas, cuando el proyecto estaba aumentando su numero de archivos. Por esto mismo hay partes del código que simplemente incumplen el principio DRY.

- Estilo: De nuevo, debido a los tiempos de entrega decidí no seguir con la creación de mi libreria propia. Sumado a que, después de hacer un par de cambios hace relativamente poco se rompieron algunos estilos.


## Puesta a punto

Antes de descargar el código debemos instalar XAMPP. A ser posible no instalar WAMPP u otro que no sea XAMPP, ya que he podido comprobar que sin XAMPP las querys no se completan satisfactoriamente.

* Una vez descargado XAMPP, copiar carpeta en C:\xampp\htdocs, o si lo tenemos en otro directorio nos dirigiremos a ese directorio y la pegaremos dentro.

**Ejemplo visual:**

![picture](\PIXCRATE\library\img\readme-images\2018-02-2123_33_56-htdocs.png)


* Abrimos el proyecto con nuestro editor favorito y nos vamos a la carpeta */core*, que se encuentra dentro de la carpeta */PIXCRATE*, y hacemos doble click en el archivo "*config.php*".

Dentro del archivo config.php encontraremos muchas cosas que no nos interesan, pero hay algunas en concreto como:

* `const p_absolute`
* `const p_port`
* `const db_config_sheet`

Estas constantes debemos cambiarlas según la necesidad.

En el caso de `const p_port`, debremos asignarle el puerto que use nuestro servidor **Apache**. En mi caso es el 8080, pero en el tuyo puede ser 80, que es el más común.

En el caso de `const p_absolute`, debremos poner la ruta de dominio y sus subcarpetas.
_Ej:_ **http://localhost:".self::p_port."/pixcrate_project/PIXCRATE**

Donde: 

`http://localhost` -> http://nombre_de_dominio (localhost para local)
`self::p_port` -> referencia a `const p_port`
`pixcrate_project` -> carpeta principal
`PIXCRATE` -> carpeta secundaria

**Ejemplo visual:**

![picture](PIXCRATE\library\img\readme-images\2018-02-2320_47_09-config.php-pixcrate_project-VisualStudioCode.png)


Una vez hecho esto debemos configurar la base de datos. Simplemente sustituir datos de la constante `const db_config_sheet`.

**NOTA**: La base de datos es el archivo `mydb_23-02-2018.sql`, que se encuentra dentro de la carpeta **PIXCRATE**, y en phpmyadmin se llama _**mydb**_.

**Ejemplo visual:**

![picture](\PIXCRATE\library\img\readme-images\2018-02-2320_51_15-config.php-pixcrate_project-VisualStudioCode.png)

En este archivo aparece la primera y unica función deprecated, que tuve que sustituir por include_once debido a que en otros equipos no funcionaba correctamente.

## Acceso

Una vez hecho esto podemos ir al buscador y poner nuestra ruta de proyecto, **contando con que tenemos XAMPP abierto**.

En mi caso queda así: http://localhost:8080/pixcrate_project/PIXCRATE/

Después, el controlador nos redireccionará a la pantalla de **Login**.

**Ejemplo visual:**

![picture](\PIXCRATE\library\img\readme-images\2018-02-2320_59_32-Login.png)

Una vez estamos aquí debemos importar la base de datos desde **phpmyadmin**.
Abrimos una pestaña nueva y escribimos el link de dirección a **phpmyadmin**.
En mi caso: http://localhost:8080/phpmyadmin/index.php

**NOTA**: La base de datos es el archivo `mydb_23-02-2018.sql`, que se encuentra dentro de la carpeta **PIXCRATE**, y en phpmyadmin se llama _**mydb**_.

También se puede acceder desde el **panel de control** de **XAMPP**, pulsando el botón **Admin** al lado de la fila de MySQL.

Nos dirijiremos nuevamente a la pantalla de login.

Donde se indica que se debe de escribir el email escribiremos _**example@example.com**_, contraseña _**example**_. Estos datos son los proporcionados por defecto con la base de datos. Poseen permisos de administrador para acceder a la sección de administración y una API Key para acceder a la API.

## Registro

Simplemente, en la pantalla de login le daremos al botón oscuro en el que pone Register y ahí podremos registrar un usuario, pero no dispondrá de permisos de administrador.

## Después del Login

Una vez estemos logueados se iniciará una sesión, y dependiendo de si nuestro usuario tiene permisos o no de administrador entrará a una sección de la página u otra.

Sin permiso: _**controlador-> home**_. Ej: http://localhost:8080/pixcrate_project/PIXCRATE/home

Con permiso: _**controlador-> management**_. Ej: http://localhost:8080/pixcrate_project/PIXCRATE/management

**Ejemplo visual:**

Página home:
![picture](PIXCRATE\library\img\readme-images\2018-02-2321_14_15-home.png)

Página management:
![picture](\PIXCRATE\library\img\readme-images\2018-02-2321_13_14-management.png)

Como vemos, aun siendo administrador, podemos entrar a la página **home**.
Si pulsamos el botón **Profile**, nos redireccionará a la sección de perfil, donde tendremos nuestra **API Key**.

Para cerrar sesión podemos pulsar el botón **Logout**, o en el panel de administración el botón **Log Out**.

## Tablas y administración

Si nos encontramos en el menú de administración, alternativamente podremos entrar a la zona de administración de _**usuarios, repositorios, imagenes, y repository-user linking**_.

No hace falta que diga que estas secciones necesitan un cambio visual. En cuanto a _**repository-user linking**_, sirve para establecer el vínculo entre usuarios y repositorios manualmente. Es un método simple, pero sinceramente no quería meterme demasiado en el charco del entorno cliente, y sí, se podría haber hecho algo más sintetizado como un simple dialog en el apartado de administración de repositorios, y de hecho lo intenté pero me daba problemas y recurrí a esta solución.

Para ver los registros de las tablas debemos pulsar en este desplegable:

 **Ejemplo visual:**

 ![picture](\PIXCRATE\library\img\readme-images\2018-02-2321_24_03-repository-user_link.png)

 Seleccionamos la opción que queramos y nos aparecerán los registros.

 Si queremos establecer un vínculo entre usuario-repositorio nos vamos al desplegable encima del repositorio y seleccionamos el usuario, la opción de visibilidad y le damos a link.

 También podemos escribir en la barra de texto arriba lo que queramos, y la tabla nos mostrará todos los resultados relacionados con esa cadena de texto introducida.

 **NOTA:** El botón Add no funciona en esta sección (pendiente de eliminar).

 Si volvemos al panel de administración y accedemos a cualquier otra zona, el funcionamiento es similar.

 En las tablas de usuario y repositorio se podrán usar tanto los botones Add, como los de Edit y Delete. También la barra de búsqueda, por supuesto.

 **NOTA:** Estos desplegables no funcionan en ninguna tabla, pretendo eliminarlos: 

 ![picture](\PIXCRATE\library\img\readme-images\2018-02-2321_31_59-desplegables.png)

**NOTA SOBRE NOTA**: La tabla **Image** no permite ni añadir, ni editar, ni borrar, ya que existe un error a la hora de ejecutar las queries. El error está en que depende del id de repositorio, pero se va a corregir para la proxima versión para que se puedan añadir las imagenes a los repositorios manualmente.

## API

De vuelta al panel de administración, podemos ver que está disponible el botón _**API**_.

Si pulsamos accederemos al panel de secciones de la **API**. Si el usuario no tuviese permisos de administrador debería introducir su clave en la barra de texto (en caso de que también tuviese permiso para acceder a la **API**, pero lamentablemente el sistema para pedir acceso a la **API** no está implementado).

Una vez en el panel de secciones de la api pulsaremos el botón **Images**.
Nos redireccionará a _**http://localhost:8080/pixcrate_project/PIXCRATE/api/images**_, la cual nos devolverá **TODAS LAS IMAGENES**. Esto va a cambiar con el tiempo, y se accederá por medio del parámetro `repositoryId` via HTTP GET.

Si queremos una imagen en concreto podemos escribir `?imageid=[id de la imágen en concreto]` después de método `images`. 

Ej: http://localhost:8080/pixcrate_project/PIXCRATE/api/images`?imageId=8239u99fj34934`

El ejemplo anterior nos devolverá:

**Ejemplo visual:**

![picture](\PIXCRATE\library\img\readme-images\2018-02-2321_41_18-imagenId.png)


# Reflexión final y conclusión

Creo que la **beta** de **PIXCRATE** me ha servido de mucha ayuda a la hora de programar con PHP, ya que me ha ayudado a entender las peticiones HTTP, las sesiones y hasta me ha enseñado a hacer una **API** propia (simple, pero ahí está), cosa que hace meses veía muy dificil y distante. En cuanto al código, me ha despertado curiosidad por la arquitectura de software, hasta el punto de que estoy estudiando bastante sobre el tema.

Se podría decir que yo ya tengo decidido que el backend es lo que me más gusta (aunque no soy lo mejor de lo mejor), pero creo que poco a poco hago bastante progreso.

Agradecimientos a mi compañero Juan Pedro que me enseñó por primera vez sobre la existencia del patrón MVC.

### Gracias y un saludo.

