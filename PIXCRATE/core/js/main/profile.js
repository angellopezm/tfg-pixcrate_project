$(document).ready(function () {
    $("#hintHolder").css("visibility", "hidden");
    assignClock();
    assignDate();
});

$(document).on("mouseenter", "#changePasswordLink", function() {
    $("#hintHolder").fadeIn("fast", function() {
        $("#hintHolder").css("visibility", "visible");
        $("#hintParagraph").html(
            `This is where you can change your password. You just need to click and fill the form up. 
            A confirmation email will be sent, and you just need to click on the link given to confirm the change.`
        );
    });
});

$(document).on("mouseenter", "#uploadProfileImageLink", function() {
    $("#hintHolder").fadeIn("fast", function() {
        $("#hintHolder").css("visibility", "visible");
        $("#hintParagraph").html(
            `This is where you can change or upload a new profile image. Click, explore your system and select the image. Then, confirm to change your profile image.`
        );
    });
});

$(document).on("mouseenter", "#accordion", function() {
    $("#hintHolder").fadeIn("fast", function() {
        $("#hintHolder").css("visibility", "visible");
        $("#hintParagraph").html(
            `Click this button to show your api key. Click again to hide it.`
        );
    });
});

$(document).on("mouseenter", "#cratesLink", function() {
    $("#hintHolder").fadeIn("fast", function() {
        $("#hintHolder").css("visibility", "visible");
        $("#hintParagraph").html(
            `This link will send you to your crate hub. Here, you can create, edit or delete your crates. 
            Also if you click one of them you will see all of your images stored in.`
        );
    });
});

$(document).on("mouseenter", "#logoutLink", function() {
    $("#hintHolder").fadeIn("fast", function() {
        $("#hintHolder").css("visibility", "visible");
        $("#hintParagraph").html(
            `Log out of Pixcrate. Say goodbye.`
        );
    });
});

$(document).on("mouseleave", "div", function() {
    $("#hintHolder").fadeOut("slow", function() {
        $("#hintHolder").css("visibility", "hidden");
        $("#hintParagraph").html(
            ""
        );
    });
});

var clockLoop = setInterval(function() {
    assignClock();
}, 1000);