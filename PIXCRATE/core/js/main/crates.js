$(document).ready(function () {





});

/* *************************** CREATE *************************** */

createDialog = $("#dialog-form-create").dialog({
    autoOpen: false,
    height: 500,
    width: 700,
    modal: true,

    buttons: {
        Erase: function () {
            $(this).find("form")[0].reset();
        },
        "Confirm Creation": function () {
            $.post("", {
                createConfirm: 1,
                repositoryName: $("#createRepositoryName").val(),
                description: $("#createDescription").val(),
                repositoryVisible: $("#createRepositoryVisible").val(),
            }, function (data) {
                
            });

        },
        Cancel: function () {
            createDialog.dialog("close");
        }

    },
    close: function () {
        createDialog.dialog("close");
    }


});

$(document).on("click", "#createCrateButton", function () {

    $("#dialog-form-create").css("display", "initial");

    createDialog.dialog("open");

});