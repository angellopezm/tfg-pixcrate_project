$(document).ready(function () {

    $("#regForm").validate({
        rules: {
            password: {
                required: true,
                minlength: 6,
            },
            passwordConfirm: {
                equalTo: "#password"
            }

        }

    });

});
