
$(function () {
    $("#birthDate").datepicker({
        dateFormat: "yy-mm-dd",
        showAnim: "slideDown",
        showButtonPanel: true,
        changeYear: true,
    });

});

$(document).ready(function () {

    $("#regForm").validate({
        rules: {
            email: {
                required: true,
                email: true,
                /*remote: {
                    type: "post",
                    url: ABS_PATH + "/check",
                    data: {
                        email: $("#email").val()
                    },
                    complete: function(data) {
                        if(data.responseText != "" && $("#email-exists").length == 0) {
                            $("#email").after("<label id='email-exists'>" + data.responseText + "</label>").addClass("pixcrate-text-dark");
                        } else {
                            $("#email-exists").remove();
                        }
                    }
                },
                */
            },
            username: {
                required: true,
                minlength: 4,
            },
            password: {
                required: true,
                minlength: 6,
            },
            passwordConfirm: {
                equalTo: "#password"
            }

        }

    });

});
