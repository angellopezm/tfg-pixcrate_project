$(document).ready(function () {

  findChanges(); // FIND CHANGES ON START OR RELOAD

  setInterval(findChanges, 1000); // REFRESH EACH 1"

  $("#search, #quantity").on("input", function (){
    findChanges();

  });

/* *************************** CREATE *************************** */

  createDialog= $("#dialog-form-create").dialog({
    autoOpen: false,
    height: 500,
    width: 700,
    modal: true,

    buttons: {
      Erase: function() {
        $(this).find("form")[0].reset();
      },
      "Confirm Creation": function(){  
        $.post("", {
          createConfirm: 1,
          repositoryName: $("#createRepositoryName").val(),
          description: $("#createDescription").val(),
          repositoryVisible: $("#createRepositoryVisible").val(),
        }, function(data){
          findChanges();
        });

      } ,
      Cancel: function() {
        createDialog.dialog("close");
      }
      
    },
    close: function() {
      createDialog.dialog("close");
    }


  });

  $(document).on("click", "#createButton", function (){
    
    $("#dialog-form-create").css("display", "initial");

    createDialog.dialog("open");

  });

  /* *************************** UPDATE *************************** */

  $(document).on("click", ".row td button.editButton", function (){
    
    $("#dialog-form-edit").css("display", "initial");

    editDialog.dialog("open");
    editDialog.rowId= $(this).parent().closest('tr').attr("id");

    //$("#email").val($("#" + dialog.rowId + " td.email").html());
    $("#editRepositoryName").val($("#" + editDialog.rowId + " td.name").html());
    $("#editRepositoryVisible").val($("#" + editDialog.rowId + " td.visible").html());
    $("#editDescription").val($("#" + editDialog.rowId + " td.description").html());

  });

  editDialog= $("#dialog-form-edit").dialog({
    autoOpen: false,
    height: 500,
    width: 700,
    modal: true,

    buttons: {
      Erase: function() {
        $(this).find("form")[0].reset();
      },
      "Confirm Edit": function(){  
        $.post("", {
          editConfirm: 1,
          repositoryId: $("#" + editDialog.rowId + " td.repositoryId").html(),
          repositoryName: $("#editRepositoryName").val(),
          description: $("#editDescription").val(),
          repositoryVisible: $("#editRepositoryVisible").val(),
        }, function(data){
          findChanges();
        });

      } ,
      Cancel: function() {
        editDialog.dialog("close");
      }
      
    },
    close: function() {
      editDialog.dialog("close");
    }
  });



  /* *************************** DELETE *************************** */
  
  $(document).on("click", ".row td button.deleteButton", function (){
    let rowId= $(this).parent().closest('tr').attr("id"); // COJO ID ANTES DE NADA

    $( function() { // ABRO DIALOGO DE CONFIRMACION
      $("#dialog-confirm").css("display", "initial");
      $("#dialog-confirm").dialog({
        resizable: false,
        height: "auto",
        width: 400,
        modal: true,
        buttons: {
          "Delete": function() { // SI DELETE -> AJAX

            $.post("",{ // PETICION AJAX
              deleteConfirm: 1,
              repositoryId: $("#" + rowId + " td.repositoryId").html(),
            },function(data){
              findChanges();
            });
        
            $(this).dialog( "close" ); // CIERRO DIALOGO
              
          },
          Cancel: function() {
            $(this).dialog( "close" );
          }
        }
      });
    });

    
  });

});

findChanges = function() {
  $.get("",{
  search: $("#search").val(),
  quantity: $("#quantity").val(),
  orderBy: $("#orderBy").val(),
  sort: $("#sort").val(),
},function(data){
  let fieldsTr= $(data).find("tr#fields")[0]; //La row de los campos
  let rowTrs= $(data).find("tr.row"); //Todas las rows de datos
  $("tr.row").remove(); //Elimina si ya hay rows  
  
  for(let i= 0; i < rowTrs.length; i++){
    $("tr").last()[0].after($(rowTrs[i])[0]); //Sustituye las rows de datos
    
  }
  
});
}