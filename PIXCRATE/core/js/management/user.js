var totalPages= 0;
var realQuantity= 0;

$(document).ready(function () {

  realQuantity= 0;

  /*$("#showMoreButton").mousedown(function (e) {
    realQuantity+= parseInt($("#quantity").val());
  });

  $("#showLessButton").mousedown(function (e) {
    realQuantity-= parseInt($("#quantity").val());
  });*/

  $( function() {
    $("#birthDate").datepicker({
      dateFormat: "yy-mm-dd",
      showAnim: "slideDown",
      showButtonPanel: true,
      changeYear: true,
    });
  
  });

  $("#createForm").validate({
    rules: {
      createEmail: {
        required: true,
        email: true,
      },
      createUsername: {
        required: true,
        minlength: 4,
      },
      createPassword: {
        required: true,
        minlength: 6,
      },
      createPasswordConfirm: {
        equalTo: "#createPassword"
      },
      createName: {
        required: true,
        minlength: 2,
      },
      createSurname: {
        required: true,
        minlength: 2,
      },
      birthDate: {
        required: true,
        date: true
      }

    }
  });

  findChanges(); // FIND CHANGES ON START OR RELOAD

  setInterval(findChanges, 1000); // REFRESH EACH 1"

  $("#search, #quantity").on("input", function (){
    findChanges();
  });

  /* *************************** CREATE *************************** */ //FIXME

  createDialog= $("#dialog-form-create").dialog({
    autoOpen: false,
    height: 500,
    width: 700,
    modal: true,

    buttons: {
      Erase: function() {
        $(this).find("form")[0].reset();
      },
      "Confirm Creation": function(){  
        $.post("", {
          createConfirm: 1,
          userEmail: $("#createEmail").val(),
          userName: $("#createUsername").val(),
          userPassword: $("#createPassword").val(),
          userFirstName: $("#createName").val(),
          userSurname: $("#createSurname").val(),
          userAdmin: $("#createServeradmin").val(),
        }, function(data){
          findChanges();
        });

      } ,
      Cancel: function() {
        createDialog.dialog("close");
      }
      
    },
    close: function() {
      createDialog.dialog("close");
    }


  });

  $(document).on("click", "#createButton", function (){
    
    $("#dialog-form-create").css("display", "initial");

    createDialog.dialog("open");

  });


  /* *************************** UPDATE *************************** */

  $(document).on("click", ".pixcrate-row td .editButton", function (){
    
    $("#dialog-form-edit").css("display", "initial");

    editDialog.dialog("open");
    editDialog.rowId= $(this).parent().closest('tr').attr("id");

    //$("#email").val($("#" + dialog.rowId + " td.email").html());
    $("#editUsername").val($("#" + editDialog.rowId + " td.username").html());
    $("#editName").val($("#" + editDialog.rowId + " td.name").html());
    $("#editSurname").val($("#" + editDialog.rowId + " td.surname").html());
    $("#editServeradmin select").val($("#" + editDialog.rowId + " td.serveradmin").html());

  });

  editDialog = $("#dialog-form-edit").dialog({
    autoOpen: false,
    height: 500,
    width: 700,
    modal: true,

    buttons: {
      Erase: function() {
        $(this).find("form")[0].reset();
      },
      "Confirm Edit": function(){  
        $.post("", {
          editConfirm: 1,
          userEmail: $("#" + editDialog.rowId + " td.email").html(),
          userName: $("#editUsername").val(),
          userPassword: $("#editPassword").val(),
          userFirstName: $("#editName").val(),
          userSurname: $("#editSurname").val(),
          userAdmin: $("#editServeradmin").val(),
        }, function(data){
            findChanges();
        });

      } ,
      Cancel: function() {
        editDialog.dialog("close");
      }
      
    },
    close: function() {
      editDialog.dialog("close");
    }
  });

  /* *************************** DELETE *************************** */
  
  $(document).on("click", ".pixcrate-row td .deleteButton", function (){
    let rowId= $(this).parent().closest('tr').attr("id"); // COJO ID ANTES DE NADA

    $( function() { // ABRO DIALOGO DE CONFIRMACION
      $("#dialog-confirm").css("display", "initial");
      $("#dialog-confirm").dialog({
        resizable: false,
        height: "auto",
        width: 400,
        modal: true,
        buttons: {
          "Delete": function() { // SI DELETE -> AJAX

            $.post("",{ // PETICION AJAX
              deleteConfirm: 1,
              userEmail: $("#" + rowId + " td.email").html(),
            },function(data){
                findChanges();
            });
        
            $(this).dialog( "close" ); // CIERRO DIALOGO            
          },
          Cancel: function() {
            $(this).dialog( "close" );
          }
        }
      });
    });
  });
});

findChanges = function() {

  /*if(parseInt($("#quantity").val()) == 0){
    realQuantity= 0;
  }*/
  
  $.get("",{
    search: $("#search").val(),
    quantity: $("#quantity").val(),
    //orderBy: $("#orderBy").val(),
    //sort: $("#sort").val(),
    //totalPages: totalPages,
  },function(data){
    let fieldsTr= $(data).find("tr#fields")[0]; //La row de los campos
    let rowTrs= $(data).find("tr.pixcrate-row"); //Todas las rows de datos
    $("tr.pixcrate-row").remove(); //Elimina si ya hay rows  
    
    for(let i= 0; i < rowTrs.length; i++){
      $("tr").last()[0].after($(rowTrs[i])[0]); //Sustituye las rows de datos
    }
    //totalPages= Math.round(rowTrs.length/parseInt($("#quantity").val()));

  });
}