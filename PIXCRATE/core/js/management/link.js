$(document).ready(function () {

  findChanges(); // FIND CHANGES ON START OR RELOAD

  //setInterval(findChanges, 1000); // REFRESH EACH 1"
  
  $("#search, #quantity").on("input", function (){
    findChanges();

  });

  $(document).on("click", ".row td button.linkButton", function (){
    let rowId= $(this).parent().closest('tr').attr("id"); // COJO ID ANTES DE NADA
    $.post("", 
    {
      linkConfirm: 1,
      user_email: $("#userSelect").val(),
      repositoryId: $("#" + rowId + " td.repositoryId").html(),
      permission: $("#" + rowId + " .permission").val(),
    },
      function (data) {
        
      }
    );

  });

});

findChanges = function() {
  $.get("",{
  search: $("#search").val(),
  quantity: $("#quantity").val(),
  orderBy: $("#orderBy").val(),
  sort: $("#sort").val(),
},function(data){
  let fieldsTr= $(data).find("tr#fields")[0]; //La row de los campos
  let rowTrs= $(data).find("tr.row"); //Todas las rows de datos
  $("tr.row").remove(); //Elimina si ya hay rows  
  
  for(let i= 0; i < rowTrs.length; i++){
    $("tr").last()[0].after($(rowTrs[i])[0]); //Sustituye las rows de datos
    
  }
  
});
}