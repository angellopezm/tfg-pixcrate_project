const GET_PORT = function() {
    if(location.port == ""){
        return "80";
    } else {
        return location.port;
    }
}

zeroIfOneDigit = function (number) {
    if(number.length < 2) {
        number = "0" + number;
    }
    return number;
}

const ABS_PATH = location.origin + ":" + GET_PORT() + location.pathname;

class Clock {
    getCurrentTimeString() {
        return new Date().toLocaleTimeString();
    };
    getCurrentUTCDateString() {
        var date = new Date();
        return date.getFullYear() + "-" + zeroIfOneDigit((date.getMonth() + 1).toString()) + "-" + zeroIfOneDigit(date.getDate().toString());
    };
}

assignClock = function () {
    $(".pixcrate-clock").html(new Clock().getCurrentTimeString());
}

assignDate = function () {
    $(".pixcrate-date").attr({
        "min": new Clock().getCurrentUTCDateString()
    });
}