<?php

define("ABS_PATH", CFGList::getAbsolutePath());
define("ROOT_DIR", isset($_SERVER["HTTPS"]) ? 'https' : 'http' . "://" . $_SERVER["SERVER_NAME"] . "/crates/");
define("ROOT_DIR_RELATIVE", "../../crates/");

/* AWS CONFIG
define("ROOT_DIR", isset($_SERVER["HTTPS"]) ? 'https' : 'http' . "://" . $_SERVER["SERVER_NAME"] . "/data/");
define("ROOT_DIR_RELATIVE", "./data/");
*/

final class CFGList
{

    private function __construct()
    {}

    const p_mode = 1; // {0: default, 1: debug}
    const p_title = "Pixcrate"; // PROJECT TITLE
    const p_authorship = "Ýngel López";

    /*########    PATHS_SHEET    ########*/
    const path_sheet =
        [
        "core_path" => "core/",
        "config_path" => "core/config.php",
        "controller_path" => "core/controllers/",
        "connection_path" => "core/connection.php",
        "model_path" => "core/models/",
        "view_path" => "core/views/",
        "connector_path" => "core/components/connector/connection.php",
        "snippet_path" => "core/snippets/html/",
        "favicon_path" => "library/favicon/",
        "hyppo_path" => "core/hyppos/",
        "style_path" => "library/css/",
        "script_path" => "core/js/",
        "img_path" => "library/img/",
    ];
    /*###################################*/

    /*########    DB_CONFIG_SHEET    ########*/
    const db_config_sheet =
        [
        "driver" => "mysql",
        "host" => "localhost",
        "user" => "root",
        "pass" => "",
        "name" => "mydb",
        "charset" => "utf8",
        "port" => "3306",
        "date_format" => "d/m/y ~ H:i",
    ];
    /*#######################################*/

    // DEPRECATED
    public static function load_dependency($key = "", $params = [""])
    {
        $s = self::path_sheet[$key];
        for ($i = 0; $i < count($params); $i++) {
            if ($i == count($params)) {
                $s .= $params[$i] . "/";
            } else {
                $s .= $params[$i];
            }

        }
        require_once $s;
    }

    public static function getAbsolutePath()
    {
        return isset($_SERVER["HTTPS"]) ? 'https' : 'http' . "://" . $_SERVER["SERVER_NAME"] . ":" .$_SERVER["SERVER_PORT"] . dirname($_SERVER["PHP_SELF"]) . "/";
    }

    public static function getCurrentLocation()
    {
        return isset($_SERVER["HTTPS"]) ? 'https' : 'http' . "://" . $_SERVER["SERVER_NAME"] . ":" .$_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    }

    /* AWS CONFIG
    public static function getAbsolutePath()
    {
        return isset($_SERVER["HTTPS"]) ? 'https' : 'http' . "://" . json_decode(file_get_contents('https://httpbin.org/ip'), true)["origin"] . ":" .$_SERVER["SERVER_PORT"] . "/";
    }

    public static function getCurrentLocation()
    {
        return isset($_SERVER["HTTPS"]) ? 'https' : 'http' . "://" . json_decode(file_get_contents('https://httpbin.org/ip'), true)["origin"] . ":" .$_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"] . "/";
    }
    */
}
