<?php 

  /**
   *  Class: PDOConnection
   *  Info: Stablish a connection with your database, taking as parameters the information stored in 'config.php' about your database.
   *  Methods: connect(), close(),
   *  Inherits: None,
   *  Implements: None,
   *  Depends: PDO, CFGList,
   */

  final class PDOConnection{

    public static $link;

    public function __construct(){}

    public static function connect(){
      try{
        self::$link= new PDO( CFGList::db_config_sheet["driver"].":dbname=".CFGList::db_config_sheet["name"].";host=".CFGList::db_config_sheet["host"].";port=".CFGList::db_config_sheet["port"], 
        CFGList::db_config_sheet["user"], CFGList::db_config_sheet["pass"]);
      }
      catch(PDOException $ex){
        die("Errno: ".$ex->getCode()."; Error: ".$ex->getMessage());
      }
      
      return self::$link;
    }

    public static function close(){
      self::$link= null;
    }

  }

?>