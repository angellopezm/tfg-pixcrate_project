<?php 

  class Application { 

    protected $controller    =    "MainController";
    protected $method        =    "actionIndex";
    protected $params        =    [];

    public function __construct(){

      require_once "core/config.php";

      $url= static::parseUrl();
      
      $controllerName= ucfirst(strtolower($url[0]))."Controller";

      if(file_exists(CFGList::path_sheet["controller_path"].$controllerName.".php")){
        $this->controller= $controllerName;
        unset($url[0]);
        
      } else{
        if($controllerName != "Controller"){
          header("Location: core/views/methoderror.php");
        }
      }

      require_once CFGList::path_sheet["controller_path"].$this->controller.".php";
      $this->controller= new $this->controller;

      if(isset($url[1])){ // Method
        $methodName= "action".ucfirst(strtolower($url[1]));

        if(method_exists($this->controller, $methodName)){
          $this->method= $methodName;
          unset($url[1]);      
        }
        else echo "<body><b style='color:red; background-color: yellow;'>HTTP 404: Method not found<b></body>";
        
      }

      $this->params= $url ? array_values($url): $this->params;

      try{
        call_user_func_array([$this->controller, $this->method], $this->params);
      }
      catch(Exception $x) { 
        echo "<body><b style='color:red; background-color: yellow;'>HTTP 404: Method not found<b></body>"; 
      }
    }

    private static function parseUrl(){      
      if(isset($_GET["url"])) return explode("/", filter_var(rtrim($_GET["url"], "/"), FILTER_SANITIZE_URL));
    }

    

  }

?>