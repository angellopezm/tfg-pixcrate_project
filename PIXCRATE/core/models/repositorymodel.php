<?php


/**
*  Class: RepositoryModel,
* 
*  Info: Model Object for Repository,
* 
*  Methods: [load, save, update, delete],
* 
*  Inherits: None,
* 
*  Implements: iModel,
* 
*  Depends: None, 
* 
*/

require_once("core/models/imodel.php");
require_once("core/connection.php");

final class RepositoryModel implements iModel{

  function __construct(){}

  /* *************************** LOAD *************************** */

  public static function load($repository= Repository::class, $tail= ""){ //TODO Generic for every object
    $connection= PDOConnection::connect();
    $statement= "SELECT * FROM ".strtolower(get_class($repository))." WHERE ";
    $query= null;
    $properties= $repository->getProperties(); // All properties from Repository
    $keys= array_keys($properties); // Variable names of Repository's properties
    $usedKeys= [];


    // FILTER PROPERTIES [NOT NULLS]
    for($i= 0; $i < count($keys); $i++){
      if($properties[$keys[$i]] != null){ // If key not null
        array_push($usedKeys, $keys[$i]);
      }
    }

    // MOUNT STATEMENT
    for($i= 0; $i < count($usedKeys); $i++){
      $statement.= $usedKeys[$i]."=? ";
      if($i < count($usedKeys)-1){
        $statement.= "AND ";
      }
    }
    $statement.= $tail;

    //PREPARE QUERY
    $query= $connection->prepare($statement);

    // BINDINGS
    for($i= 0; $i < count($usedKeys); $i++){
      $property= $usedKeys[$i];
      @$query->bindParam($i+1, $repository->$property); // It works but I had to supress the "Notice" message
    }

    $query->execute();
    $result= $query->fetch(PDO::FETCH_ASSOC);
    return $result;

    PDOConnection::close();
  }

  public static function loadAll($repository= Repository::class, $tail= ""){ //TODO Generic for every object
    $connection= PDOConnection::connect();
    $statement= "SELECT * FROM ".strtolower(get_class($repository))." WHERE ";
    $query= null;
    $properties= $repository->getProperties(); // All properties from Repository
    $keys= array_keys($properties); // Variable names of Repository's properties
    $usedKeys= [];


    // FILTER PROPERTIES [NOT NULLS]
    for($i= 0; $i < count($keys); $i++){
      if($properties[$keys[$i]] != null){ // If key not null
        array_push($usedKeys, $keys[$i]);
      }
    }

    // MOUNT STATEMENT
    for($i= 0; $i < count($usedKeys); $i++){
      $statement.= $usedKeys[$i]."=? ";
      if($i < count($usedKeys)-1){
        $statement.= "AND ";
      }
    }
    $statement.= $tail;

    //PREPARE QUERY
    $query= $connection->prepare($statement);

    // BINDINGS
    for($i= 0; $i < count($usedKeys); $i++){
      $property= $usedKeys[$i];
      @$query->bindParam($i+1, $repository->$property); // It works but I had to supress the "Notice" message
    }

    $query->execute();
    $result= $query->fetchAll(PDO::FETCH_ASSOC);
    return $result;

    PDOConnection::close();
  }

  public static function loadAnyContaining($word= "", $tail= ""){ //TODO Generic for every object
    $connection= PDOConnection::connect();
    $repository= new Repository();
    $statement= "SELECT * FROM ".strtolower(get_class($repository))." WHERE ";

    $query= null;
    $properties= $repository->getProperties(); // All properties from Repository
    $keys= array_keys($properties); // Variable names of Repository's properties
    
    // MOUNT STATEMENT
    for($i= 0; $i < count($keys); $i++){
      $statement.= $keys[$i]." LIKE ? ";
      if($i < count($keys)-1){
        $statement.= "OR ";
      }
    }
    $statement.= $tail;

    //PREPARE QUERY
    $query= $connection->prepare($statement);

    // BINDINGS
    for($i= 0; $i < count($keys); $i++){
      $param= "%".$word."%";
      @$query->bindParam($i+1, $param); // It works but I had to supress the "Notice" message
    }

    print_r($statement);
    $query->execute();
    $result= $query->fetchAll(PDO::FETCH_ASSOC);
    return $result;
    PDOConnection::close();
  }

  /* *************************** CREATE *************************** */

  public static function save($repository= Repository::class, $tail= ""){
    $connection = PDOConnection::connect();
    $statement= "INSERT INTO ".strtolower(get_class($repository))." ( ";
    $query = null;
    $properties= $repository->getProperties(); // All properties from Repository
    $keys= array_keys($properties); // Variable names of Repository's properties
    $usedKeys= [];

    // FILTER PROPERTIES [NOT NULLS]
    for($i= 0; $i < count($keys); $i++){
      if($properties[$keys[$i]] != null){ // If key not null
        array_push($usedKeys, $keys[$i]);
      }
    }

    // MOUNT STATEMENT
    for($i= 0; $i < count($usedKeys); $i++){
      $statement.= $usedKeys[$i];
      if($i < count($usedKeys)-1){
        $statement.= ", ";
      }
      else if($i == count($usedKeys)-1){
        $statement.= " )";
      }
    }
    $statement.= " VALUES (";

    for($i= 0; $i < count($usedKeys); $i++){
      $statement.= "?";
      if($i < count($usedKeys)-1){
        $statement.= ", ";
      }
      else if($i == count($usedKeys)-1){
        $statement.= " ) ";
      }
    }
    $statement.= $tail;

    

    //PREPARE QUERY
    $query= $connection->prepare($statement);

    // BINDINGS
    for($i= 0; $i < count($usedKeys); $i++){
      $property= $usedKeys[$i];
      @$query->bindParam($i+1, $repository->$property); // It works but I had to supress the "Notice" message
      
    }

    $query->execute();
    $result= $query->fetchAll(PDO::FETCH_ASSOC);

    PDOConnection::close();

  }

  /* *************************** UPDATE *************************** */

  public static function update($repository= Repository::class, $tail= ""){
    $connection = PDOConnection::connect();
    $statement= "UPDATE ".strtolower(get_class($repository))." SET ";
    $query = null;
    $properties= $repository->getProperties(); // All properties from Repository
    $keys= array_keys($properties); // Variable names of Repository's properties
    $usedKeys= [];

    // FILTER PROPERTIES [NOT NULLS]
    for($i= 0; $i < count($keys); $i++){
      if($properties[$keys[$i]] != null){ // If key not null
        array_push($usedKeys, $keys[$i]);
      }
    }

    // MOUNT STATEMENT
    for($i= 0; $i < count($usedKeys); $i++){
      $statement.= $usedKeys[$i]."=?";
      if($i < count($usedKeys)-1){
        $statement.= ",";
      }
      else{
        $statement.= " ";
      }
      
    }
    $statement.= $tail;

    // GET PKS QUERY 
    $tableKeys = "SHOW KEYS FROM ".strtolower(get_class($repository))." WHERE Key_name = 'PRIMARY'";
    //PREPARE PKS QUERY
    $query= $connection->prepare($tableKeys);
    $query->execute();

    $result= $query->fetchAll(PDO::FETCH_ASSOC);

    //PREPARE QUERY
    $query= $connection->prepare($statement);

    // BINDINGS
    for($i= 0; $i < count($usedKeys); $i++){
      $property= $usedKeys[$i];
      @$query->bindParam($i+1, $repository->$property); // It works but I had to supress the "Notice" message

      if($i+1 >= count($usedKeys)){
        @$query->bindParam(count($usedKeys) + 1, $repository->__get($result[0]["Column_name"]));
      }
    }
    $query->execute();

    PDOConnection::close();

  }


  /* *************************** DELETE *************************** */
  public static function delete($repository= Repository::class, $tail= "") {
    $connection = PDOConnection::connect();
    $statement= "DELETE FROM ".strtolower(get_class($repository))." WHERE ";
    $query = null;
    $properties= $repository->getProperties(); // All properties from Repository
    $keys= array_keys($properties); // Variable names of Repository's properties
    $usedKeys= [];

    // FILTER PROPERTIES [NOT NULLS]
    for($i= 0; $i < count($keys); $i++){
      if($properties[$keys[$i]] != null){ // If key not null
        array_push($usedKeys, $keys[$i]);
      }
    }

    // MOUNT STATEMENT
    for($i= 0; $i < count($usedKeys); $i++){
      $statement.= $usedKeys[$i]."=?";
      if($i < count($usedKeys)-1){
        $statement.= "AND ";
      }
      
    }
    $statement.= $tail;

    //PREPARE QUERY
    $query= $connection->prepare($statement);

    // BINDINGS
    for($i= 0; $i < count($usedKeys); $i++){
      $property= $usedKeys[$i];
      @$query->bindParam($i+1, $repository->$property); // It works but I had to supress the "Notice" message
    }

    $query->execute();

    PDOConnection::close();

  }
  
  /* *************************** MISC *************************** */

  public static function selfSet($repository = Repository::class) {
    foreach ($repository as $key => $value) {
      if(!isset($value)){
        return false;
      }
    } 
  }

  private static function hasNull($element) {
    foreach($element as $key => $value) {
      if($value == null){
        return true;
      }
      else{
        return false;
      }
    }
  }

}