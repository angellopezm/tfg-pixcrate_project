<?php


/**
*  Class: CommentModel,
* 
*  Info: Model Object for Comment,
* 
*  Methods: [load, save, update, delete],
* 
*  Inherits: None,
* 
*  Implements: iModel,
* 
*  Depends: None, 
* 
*/

require_once("core/models/imodel.php");
require_once("core/connection.php");

final class CommentModel implements iModel{

  function __construct(){}

  /* *************************** LOAD *************************** */

  public static function load($comment= Comment::class, $tail= ""){ //TODO Generic for every object
    $connection= PDOConnection::connect();
    $statement= "SELECT * FROM ".strtolower(get_class($comment))." WHERE ";
    $query= null;
    $properties= $comment->getProperties(); // All properties from comment
    $keys= array_keys($properties); // Variable names of comment's properties
    $usedKeys= [];


    // FILTER PROPERTIES [NOT NULLS]
    for($i= 0; $i < count($keys); $i++){
      if($properties[$keys[$i]] != null){ // If key not null
        array_push($usedKeys, $keys[$i]);
      }
    }

    // MOUNT STATEMENT
    for($i= 0; $i < count($usedKeys); $i++){
      $statement.= $usedKeys[$i]."=? ";
      if($i < count($usedKeys)-1){
        $statement.= "AND ";
      }
    }
    $statement.= $tail;

    //PREPARE QUERY
    $query= $connection->prepare($statement);

    // BINDINGS
    for($i= 0; $i < count($usedKeys); $i++){
      $property= $usedKeys[$i];
      @$query->bindParam($i+1, $comment->$property); // It works but I had to supress the "Notice" message
    }

    $query->execute();
    $result= $query->fetch(PDO::FETCH_ASSOC);
    return $result;
    PDOConnection::close();
  }

  public static function loadAnyContaining($word= "", $tail= ""){ //TODO Generic for every object
    $connection= PDOConnection::connect();
    $comment= new Comment();
    $statement= "SELECT * FROM ".strtolower(get_class($comment))." WHERE ";

    $query= null;
    $properties= $comment->getProperties(); // All properties from comment
    $keys= array_keys($properties); // Variable names of comment's properties
    
    // MOUNT STATEMENT
    for($i= 0; $i < count($keys); $i++){
      $statement.= $keys[$i]." LIKE ? ";
      if($i < count($keys)-1){
        $statement.= "OR ";
      }
    }
    $statement.= $tail;

    //PREPARE QUERY
    $query= $connection->prepare($statement);

    // BINDINGS
    for($i= 0; $i < count($keys); $i++){
      $param= "%".$word."%";
      @$query->bindParam($i+1, $param); // It works but I had to supress the "Notice" message
    }

    $query->execute();
    $result= $query->fetchAll(PDO::FETCH_ASSOC);
    return $result;
    PDOConnection::close();
  }

  public static function loadAll($comment= Comment::class, $tail= ""){ //TODO Generic for every object
    $connection= PDOConnection::connect();
    $statement= "SELECT * FROM ".strtolower(get_class($comment))." WHERE ";
    $query= null;
    $properties= $comment->getProperties(); // All properties from comment
    $keys= array_keys($properties); // Variable names of comment's properties
    $usedKeys= [];


    // FILTER PROPERTIES [NOT NULLS]
    for($i= 0; $i < count($keys); $i++){
      if($properties[$keys[$i]] != null){ // If key not null
        array_push($usedKeys, $keys[$i]);
      }
    }

    // MOUNT STATEMENT
    for($i= 0; $i < count($usedKeys); $i++){
      $statement.= $usedKeys[$i]."=? ";
      if($i < count($usedKeys)-1){
        $statement.= "AND ";
      }
    }
    $statement.= $tail;

    //PREPARE QUERY
    $query= $connection->prepare($statement);

    // BINDINGS
    for($i= 0; $i < count($usedKeys); $i++){
      $property= $usedKeys[$i];
      @$query->bindParam($i+1, $comment->$property); // It works but I had to supress the "Notice" message
    }

    $query->execute();
    $result= $query->fetchAll(PDO::FETCH_ASSOC);
    return $result;
    PDOConnection::close();
  }

  /* *************************** CREATE *************************** */

  public static function save($comment= Comment::class, $tail= ""){
    $connection = PDOConnection::connect();
    $statement= "INSERT INTO ".strtolower(get_class($comment))." ( ";
    $query = null;
    $properties= $comment->getProperties(); // All properties from comment
    $keys= array_keys($properties); // Variable names of comment's properties
    $usedKeys= [];

    // FILTER PROPERTIES [NOT NULLS]
    for($i= 0; $i < count($keys); $i++){
      if($properties[$keys[$i]] != null){ // If key not null
        array_push($usedKeys, $keys[$i]);
      }
    }

    // MOUNT STATEMENT
    for($i= 0; $i < count($usedKeys); $i++){
      $statement.= $usedKeys[$i];
      if($i < count($usedKeys)-1){
        $statement.= ", ";
      }
      else if($i == count($usedKeys)-1){
        $statement.= " )";
      }
    }
    $statement.= " VALUES (";

    for($i= 0; $i < count($usedKeys); $i++){
      $statement.= "?";
      if($i < count($usedKeys)-1){
        $statement.= ", ";
      }
      else if($i == count($usedKeys)-1){
        $statement.= " ) ";
      }
    }
    $statement.= $tail;

    //PREPARE QUERY
    $query= $connection->prepare($statement);

    // BINDINGS
    for($i= 0; $i < count($usedKeys); $i++){
      $property= $usedKeys[$i];
      @$query->bindParam($i+1, $comment->$property); // It works but I had to supress the "Notice" message
      
    }

    $query->execute();
    $result= $query->fetchAll(PDO::FETCH_ASSOC);

    PDOConnection::close();

  }

  /* *************************** UPDATE *************************** */

  public static function update($comment= Comment::class, $tail= ""){
    $connection = PDOConnection::connect();
    $statement= "UPDATE ".strtolower(get_class($comment))." SET ";
    $query = null;
    $properties= $comment->getProperties(); // All properties from comment
    $keys= array_keys($properties); // Variable names of comment's properties
    $usedKeys= [];

    // FILTER PROPERTIES [NOT NULLS]
    for($i= 0; $i < count($keys); $i++){
      if($properties[$keys[$i]] != null){ // If key not null
        array_push($usedKeys, $keys[$i]);
      }
    }

    // MOUNT STATEMENT
    for($i= 0; $i < count($usedKeys); $i++){
      $statement.= $usedKeys[$i]."=?";
      if($i < count($usedKeys)-1){
        $statement.= ",";
      }
      else{
        $statement.= " ";
      }
      
    }
    $statement.= $tail;

    // GET PKS QUERY 
    $tableKeys = "SHOW KEYS FROM ".strtolower(get_class($comment))." WHERE Key_name = 'PRIMARY'";
    //PREPARE PKS QUERY
    $query= $connection->prepare($tableKeys);
    $query->execute();

    $result= $query->fetchAll(PDO::FETCH_ASSOC);

    //PREPARE QUERY
    $query= $connection->prepare($statement);

    // BINDINGS
    for($i= 0; $i < count($usedKeys); $i++){
      $property= $usedKeys[$i];
      @$query->bindParam($i+1, $comment->$property); // It works but I had to supress the "Notice" message

      if($i+1 >= count($usedKeys)){
        @$query->bindParam(count($usedKeys) + 1, $comment->__get($result[0]["Column_name"]));
      }
    }
    
    

    $query->execute();
    $result= $query->fetchAll(PDO::FETCH_ASSOC);

    PDOConnection::close();
    return $result;
  }


  /* *************************** DELETE *************************** */
  public static function delete($comment= Comment::class, $tail= "") {
    $connection = PDOConnection::connect();
    $statement= "DELETE FROM ".strtolower(get_class($comment))." WHERE ";
    $query = null;
    $properties= $comment->getProperties(); // All properties from comment
    $keys= array_keys($properties); // Variable names of comment's properties
    $usedKeys= [];

    // FILTER PROPERTIES [NOT NULLS]
    for($i= 0; $i < count($keys); $i++){
      if($properties[$keys[$i]] != null){ // If key not null
        array_push($usedKeys, $keys[$i]);
      }
    }

    // MOUNT STATEMENT
    for($i= 0; $i < count($usedKeys); $i++){
      $statement.= $usedKeys[$i]."=?";
      if($i < count($usedKeys)-1){
        $statement.= "AND ";
      }
      
    }
    $statement.= $tail;

    //PREPARE QUERY
    $query= $connection->prepare($statement);

    // BINDINGS
    for($i= 0; $i < count($usedKeys); $i++){
      $property= $usedKeys[$i];
      @$query->bindParam($i+1, $comment->$property); // It works but I had to supress the "Notice" message
    }

    $query->execute();
    $result= $query->fetchAll(PDO::FETCH_ASSOC);

    PDOConnection::close();

  }
  
  /* *************************** MISC *************************** */

  public static function selfSet($comment = Comment::class) {
    foreach ($comment as $key => $value) {
      if(!isset($value)){
        return false;
      }
    } 
  }

  private static function hasNull($element) {
    foreach($element as $key => $value) {
      if($value == null){
        return true;
      }
      else{
        return false;
      }
    }
  }

}
?>