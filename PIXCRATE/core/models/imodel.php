<?php

interface iModel{
    static function load($params= []);
    static function save($params= []);
    static function update($params= []);
    static function delete($params= []);
    static function selfSet();
}