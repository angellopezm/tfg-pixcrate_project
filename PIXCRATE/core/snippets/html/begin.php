<!DOCTYPE html>
<html lang="en">

    <head>

        <link rel="stylesheet" href="<?=ABS_PATH . CFGList::path_sheet['style_path']?>bootstrap.css">

        <link rel="stylesheet" href="<?=ABS_PATH . CFGList::path_sheet['style_path']?>main.css">
        <link rel="icon" href="<?=CFGList::path_sheet['favicon_path'] . 'icon.png'?>" type="image/png" sizes="32x32">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <script src="<?=ABS_PATH . CFGList::path_sheet['script_path']?>jquery-3.3.1.js"></script>
        <script src="<?=ABS_PATH . CFGList::path_sheet['script_path']?>pixcrate-main.js"></script> <!--PIXCRTE CORE JS-->
        <script src="<?=ABS_PATH . CFGList::path_sheet['script_path']?>bootstrap.js"></script>

        <title><?=CFGList::p_title?></title>
    </head>

    <body class="pixcrate-gray-light-4">
        <div class="p-absolute w-100 h-100 mb-5">
            <div class="container-fluid p-0 w-100 h-100">