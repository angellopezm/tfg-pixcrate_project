<?php

require_once "core/models/usermodel.php";
require_once "core/hyppos/user.php";


$email = isset($_POST["email"]) ? $_POST["email"] : null;

if(isset($email)) {
    $user = new User(array("email" => $email));

    $result = UserModel::loadAll($user);

    if(sizeof($result) > 0) {
        echo "This email is already in use";
    }
}