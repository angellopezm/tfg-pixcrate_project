<?php

require_once("core/controllers/CFoundation.php");

class ProfileController extends CFoundation {
  public function actionIndex() {
    require CFGList::path_sheet["snippet_path"]."begin.php";
    parent::render("user-side/profile");
    require CFGList::path_sheet["snippet_path"]."end.php";
  }
  public function actionCrates() {
    require CFGList::path_sheet["snippet_path"]."begin.php";
    parent::render("user-side/crates");
    require CFGList::path_sheet["snippet_path"]."end.php";
  }
  public function actionCreate_repository() {
    require CFGList::path_sheet["snippet_path"]."begin.php";
    parent::render("user-side/crates/addCrate");
    require CFGList::path_sheet["snippet_path"]."end.php";
  }
  public function actionEdit_repository() {
    require CFGList::path_sheet["snippet_path"]."begin.php";
    parent::render("user-side/crates/editCrate");
    require CFGList::path_sheet["snippet_path"]."end.php";
  }
  public function actionDelete_repository() {
    require CFGList::path_sheet["snippet_path"]."begin.php";
    parent::render("user-side/crates/deleteCrate");
    require CFGList::path_sheet["snippet_path"]."end.php";
  }
  public function actionProfile_image() {
    require CFGList::path_sheet["snippet_path"]."begin.php";
    parent::render("user-side/profileimage");
    require CFGList::path_sheet["snippet_path"]."end.php";
  }
}


?>