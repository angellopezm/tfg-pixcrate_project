<?php

  require_once("core/controllers/CFoundation.php");

  class MainController extends CFoundation {
    public function actionIndex(){
      require CFGList::path_sheet["snippet_path"]."begin.php";
      parent::render("user-side/main");
      require CFGList::path_sheet["snippet_path"]."end.php";
    }
  }

?>