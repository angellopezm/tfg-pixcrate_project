<?php

require_once("core/controllers/CFoundation.php");

class HomeController extends CFoundation {
  public function actionIndex(){
    require CFGList::path_sheet["snippet_path"]."begin.php";
    parent::render("user-side/home");
    require CFGList::path_sheet["snippet_path"]."end.php";
  }
}

?>