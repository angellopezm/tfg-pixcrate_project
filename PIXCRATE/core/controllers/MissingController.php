<?php

require_once("core/controllers/CFoundation.php");

class MissingController extends CFoundation {
  public function actionIndex(){
    require CFGList::path_sheet["snippet_path"]."begin.php";
    require_once "core/utils.php";
    Utils::startSession()->checkSession();
    echo "<h1>HTTP 404: URI Error</h1>";
    require CFGList::path_sheet["snippet_path"]."end.php";
  }

  public function action404Image() {
    require CFGList::path_sheet["snippet_path"]."begin.php";
    require_once "core/utils.php";
    require CFGList::path_sheet["snippet_path"]."end.php";
  }
}

?>