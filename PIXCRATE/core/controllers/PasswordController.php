<?php

require_once("core/controllers/CFoundation.php");

class PasswordController extends CFoundation {
  public function actionIndex(){
    require CFGList::path_sheet["snippet_path"]."begin.php";
    parent::render("public-side/emailrequest");
    require CFGList::path_sheet["snippet_path"]."end.php";
  }
  public function actionChange(){
    require CFGList::path_sheet["snippet_path"]."begin.php";
    parent::render("public-side/changepassword");
    require CFGList::path_sheet["snippet_path"]."end.php";
  }

}

?>