<?php

require_once("core/controllers/CFoundation.php");

class CrateController extends CFoundation {
  public function actionIndex() {
    require CFGList::path_sheet["snippet_path"]."begin.php";
    parent::render("user-side/crate");
    require CFGList::path_sheet["snippet_path"]."end.php";
  }

  public function actionImage() {
    require CFGList::path_sheet["snippet_path"]."begin.php";
    parent::render("user-side/image");
    require CFGList::path_sheet["snippet_path"]."end.php";
  }

  public function actionUpload() {
    require CFGList::path_sheet["snippet_path"]."begin.php";
    parent::render("user-side/images/addImage");
    require CFGList::path_sheet["snippet_path"]."end.php";
  }
  
  public function actionDelete_image() {
    require CFGList::path_sheet["snippet_path"]."begin.php";
    parent::render("user-side/images/deleteImage");
    require CFGList::path_sheet["snippet_path"]."end.php";
  }
  
  public function actionEdit_image() {
    require CFGList::path_sheet["snippet_path"]."begin.php";
    parent::render("user-side/images/editImage");
    require CFGList::path_sheet["snippet_path"]."end.php";
  }
}


?>