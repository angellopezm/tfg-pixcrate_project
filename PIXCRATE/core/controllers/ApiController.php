<?php

require_once("core/controllers/CFoundation.php");

class ApiController extends CFoundation {
  public function actionIndex(){
    require CFGList::path_sheet["snippet_path"]."begin.php";
    parent::render("api/verify");
    require CFGList::path_sheet["snippet_path"]."end.php";
  }

  public function actionMainpanel(){
    require CFGList::path_sheet["snippet_path"]."begin.php";
    parent::render("api/mainpanel");
    require CFGList::path_sheet["snippet_path"]."end.php";
  }

  public function actionImages(){
    parent::render("api/images");
  }
}
?>