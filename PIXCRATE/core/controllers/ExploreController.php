<?php

require_once("core/controllers/CFoundation.php");

class ExploreController extends CFoundation {
  public function actionIndex(){
    require CFGList::path_sheet["snippet_path"]."begin.php";
    parent::render("public-side/searchresults");
    require CFGList::path_sheet["snippet_path"]."end.php";
  }
  public function actionImages(){
    require CFGList::path_sheet["snippet_path"]."begin.php";
    parent::render("public-side/explore/images");
    require CFGList::path_sheet["snippet_path"]."end.php";
  }
  public function actionCrates(){
    require CFGList::path_sheet["snippet_path"]."begin.php";
    parent::render("public-side/explore/crates");
    require CFGList::path_sheet["snippet_path"]."end.php";
  }
  public function actionUsers(){
    require CFGList::path_sheet["snippet_path"]."begin.php";
    parent::render("public-side/explore/profiles");
    require CFGList::path_sheet["snippet_path"]."end.php";
  }
}

?>