<?php

require_once("core/controllers/CFoundation.php");

class LogoutController extends CFoundation {
  public function actionIndex(){
    include_once "core/snippets/session/session_end.php";
  }
}

?>