<?php

require_once("core/controllers/CFoundation.php");

class RegisterController extends CFoundation {
  public function actionIndex(){
    require CFGList::path_sheet["snippet_path"]."begin.php";
    parent::render("user-side/register");
    require CFGList::path_sheet["snippet_path"]."end.php";
  }

  public function actionCheck(){
    require_once "core/snippets/html/checkExistence.php";
  }
}

?>