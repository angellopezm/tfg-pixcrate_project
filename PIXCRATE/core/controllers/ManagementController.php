<?php

require_once("core/controllers/CFoundation.php");

class ManagementController extends CFoundation {
  public function actionIndex(){
    require CFGList::path_sheet["snippet_path"]."begin.php";
    parent::render("management/management");
    require CFGList::path_sheet["snippet_path"]."end.php";
  }
  public function actionUser(){
    require CFGList::path_sheet["snippet_path"]."begin.php";
    parent::render("management/usermanagement");
    require CFGList::path_sheet["snippet_path"]."end.php";
  }
  public function actionRepository(){
    require CFGList::path_sheet["snippet_path"]."begin.php";
    parent::render("management/repositorymanagement");
    require CFGList::path_sheet["snippet_path"]."end.php";
  }

  public function actionImage(){
    require CFGList::path_sheet["snippet_path"]."begin.php";
    parent::render("management/imagemanagement");
    require CFGList::path_sheet["snippet_path"]."end.php";
  }

  public function actionLink(){
    require CFGList::path_sheet["snippet_path"]."begin.php";
    parent::render("management/link");
    require CFGList::path_sheet["snippet_path"]."end.php";
  }
}



?>