<?php

interface iObject {
  function __construct();
  function __get($property);
  function __set($property, $value);
  static function notEmpty();
  function getProperties();
}