<?php

  /**
   *  Class: User,
   * 
   *  Info: HyperText Preprocessor Plain Object for User,
   * 
   *  Methods: [__get(), __set(), notEmpty(), getProperties()],
   * 
   *  Inherits: None,
   * 
   *  Implements: None,
   * 
   *  Depends: None, 
   * 
   */

  abstract class Object {

    function __construct($attributes= ["" => ""]){
      $keys= array_keys($attributes);
      $properties= $this->getProperties();
      foreach($properties as $key => $value){
        if(in_array($key, $keys, true)){
          $this->$key = $attributes[$keys[array_search($key, $keys, true)]];
        }
      }
      return $this;
    }

    public function __get($property){
      if (property_exists($this, $property)) {
        return $this->$property;
      }
    }

    public function __set($property, $value) {
      if (property_exists($this, $property)) {
        $this->$property = $value;
      }
  
      return $this;
    }

    public static function notEmpty() {
      foreach (self as $key => $value) {
        if($value == null){
          return false;
        }
      } 
    }

    public function getProperties(){
      return get_object_vars($this);
    }

  }

