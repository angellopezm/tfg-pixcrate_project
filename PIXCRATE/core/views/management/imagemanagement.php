<?php
  require_once("core/hyppos/image.php");
  require_once("core/models/imagemodel.php");
  require_once("core/utils.php");

  if(!isset($_SESSION)){
    session_start();
  }

  if(!isset($_SESSION["id"])){
    header("Location:".CFGList::p_absolute."/");
  } else if(isset($_SESSION["serveradmin"]) && $_SESSION["serveradmin"] != 1){
    header("Location:".CFGList::p_absolute."/home");
  }

?>

<head>
  <script src="<?=CFGList::p_absolute."/".CFGList::path_sheet['script_path']?>jquery.validate.js"></script>
  <script src="<?=CFGList::p_absolute."/".CFGList::path_sheet['script_path']?>management/image.js"></script>
  <script src="<?=CFGList::p_absolute."/".CFGList::path_sheet['script_path']?>jquery-ui-1.12.1.custom/jquery-ui.js"></script>
</head>

<body>
  
  <!-- *************************** CREATE *************************** -->

  <div id="dialog-form-create" title="Create a new row" style="display:none;">
    <p class="validateTips">All form fields are required.</p>
  
    <form id="createForm">
      <fieldset>

        <label for="createTitle">Title</label>
        <input type="text" name="createTitle" id="createTitle" class="text ui-widget-content ui-corner-all"> <br>

        <label for="createDescription">Description</label>
        <!--<input type="text" name="createDescription" id="createDescription" class="text ui-widget-content ui-corner-all"> <br> -->
        <textarea name="createDescription" id="createDescription" cols="30" rows="2" class="text ui-widget-content ui-corner-all"></textarea> <br>

        <label for="createVisibility">Name</label>
        <select name="createVisibility" id="createVisibility">
        <option value="null">Select</option>
          <option value="PRIVATE">Private</option>
          <option value="HIDDEN">Hidden</option>
          <option value="PUBLIC">Public</option>
        </select> <br>

        <label for="createProgrammedDateTime">Programmed DateTime</label>
        <input type="datetime-local" name="createProgrammedDateTime" id="createProgrammedDateTime" class="text ui-widget-content ui-corner-all"> <br>
        
        <!-- Allow form submission with keyboard without duplicating the dialog button -->
        <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
      </fieldset>
    </form>
  </div>


  <!-- *************************** UPDATE *************************** -->

  <div id="dialog-form-edit" title="Edit this row" style="display:none;">
    <p class="validateTips">All form fields are required.</p>
  
    <form >
      <fieldset>

        <label for="editTitle">Title</label>
        <input type="text" name="editTitle" id="editTitle" class="text ui-widget-content ui-corner-all"> <br>

        <label for="editDescription">Description</label>
        <textarea name="editDescription" id="editDescription" cols="30" rows="2" class="text ui-widget-content ui-corner-all"></textarea> <br>

        <label for="editVisibility">Name</label>
        <select name="editVisibility" id="editVisibility">
        <option value="null">Select</option>
          <option value="PRIVATE">Private</option>
          <option value="HIDDEN">Hidden</option>
          <option value="PUBLIC">Public</option>
        </select> <br>

        <label for="editProgrammedDateTime">Programmed DateTime</label>
        <input type="datetime" name="editProgrammedDateTime" id="editProgrammedDateTime" class="text ui-widget-content ui-corner-all"> <br>
      
        <!-- Allow form submission with keyboard without duplicating the dialog button -->
        <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
      </fieldset>
    </form>
  </div>

  <!-- *************************** DELETE *************************** -->

  <div id="dialog-confirm" title="Delete selected" style="display: none;">
    <p><span style="float:left; margin:12px 12px 20px 0;"></span>This row is going to dissapear. Are you sure?</p>
  </div>

  <?php

    $search= isset($_GET["search"])? $_GET["search"] : null;
    $quantity= isset($_GET["quantity"])? $_GET["quantity"] : null;

    $result= ImageModel::loadAnyContaining($search, "LIMIT ".$quantity);
  ?>     

  <?php
    /* *************************** CREATE *************************** */ //FIXME no funciona debido a que no disponemos de id de repositorio aún.
    $createConfirmation= isset($_POST["createConfirm"])? $_POST["createConfirm"] : null;
    if($createConfirmation == 1) {
      $image= new Image(array("imageId" => Utils::generateId(rand(1, 45)), /*"repository_repositoryId" => $_POST["repositoryId"]*/ "url" => "sampleUrl",
      "title" => $_POST["createTitle"], "date" => date("Y-m-d"), "description" => $_POST["createDescription"], "visibility" => $_POST["createVisibility"], 
      "programmedDateTime" => $_POST["createProgrammedDateTime"]));
      $result= ImageModel::save($image);
    }

    /* *************************** DELETE *************************** */
    $deleteConfirmation= isset($_POST["deleteConfirm"])? $_POST["deleteConfirm"] : null;
    if($deleteConfirmation == 1) {
      $image= new Image(array("imageId" => $_POST["imageId"]));
      ImageModel::delete($image);
    }

    /* *************************** UPDATE *************************** */
    $editConfirmation= isset($_POST["editConfirm"])? $_POST["editConfirm"] : null;
    if($editConfirmation == 1) {
      $image= new Image(array("imageId" => $_POST["imageId"], "title" => $_POST["editTitle"], "description" => $_POST["editDescription"], "visibility" => $_POST["editVisibility"], 
      "programmedDateTime" => $_POST["editProgrammedDateTime"]));
      $result= ImageModel::update($image, "WHERE imageId=?");
    }


  ?>
  <table class="dark-table default-shadow" id="table">
    <tr id="fields">
      <th>Id</th>
      <th>Repository Id</th>
      <th>URL</th>
      <th>Title</th>
      <th>Date</th>
      <th>Description</th>
      <th>Likes</th>
      <th>Reposts</th>
      <th>Visibility</th>
      <th>Will be published on:</th>
      <th style="display: flex; position: relative; flex-wrap: wrap;" class="default-search-bar">
        <input type="text" name="search" placeholder="Search emails, usernames, names..." class="default-font" id="search">
        <button id="createButton" class="default-button">Add</button>
        <form method="get">
            <select name="quantity" id="quantity">
                  <option value="0" default>0</option>
                  <option value="5">5</option>
                  <option value="25">25</option>
                  <option value="50">50</option>
                  <option value="100">100</option>
                  <option value="200">200</option>
                  <option value="300">300</option>
            </select>
            <select name="orderBy" id="orderBy">
                  <option value="" default>Select</option>
                  <option value="imageId">Id</option>
                  <option value="repository_repositoryId">Repository Id</option>
                  <option value="title">Title</option>
                  <option value="date">Date</option>
                  <option value="description">Description</option>
                  <option value="like">Likes</option>
                  <option value="repost">Reposts</option>
                  <option value="visibility">Visibility</option>
                  <option value="programmedDateTime">Programmed Date Time</option>
            </select>
            <select name="sort" id="sort">
                  <option value="" default>Select</option>
                  <option value="ASC">ASC</option>
                  <option value="DESC">DESC</option>
            </select>
        </form>
      </th>
    </tr>
    <div id="userList">
      <?php
        $i= -1; //FOR KNOWING THE ITERATION NUMBER
        foreach($result as $array){
          $i++; 
          echo "<tr class='row' id='".$i."'>".
          "<td class='imageId'>".$array["imageId"]."</td>".
          "<td class='repository_repositoryId'>".$array["repository_repositoryId"]."</td>".
          "<td class='url'><a href='".$array["url"]."'>Open</td>".
          "<td class='title'>".$array["title"]."</td>".
          "<td class='date'>".$array["date"]."</td>".
          "<td class='description'>".$array["description"]."</td>".
          "<td class='like'>".$array["like"]."</td>".
          "<td class='repost'>".$array["repost"]."</td>".
          "<td class='visibility'>".$array["visibility"]."</td>".
          "<td class='programmedDateTime'>".$array["programmedDateTime"]."</td>".
          "<td><button class='editButton default-button'>Edit</button><button class='deleteButton default-button'>Delete</button></td>".
          "</tr>";
          
        }
      ?>
    </div>

  </table>
  
</body>