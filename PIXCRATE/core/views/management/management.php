<head>
  <link rel="stylesheet" href="<?=ABS_PATH . CFGList::path_sheet['style_path']?>management-main.css">
</head>

<?php
require_once "core/utils.php";

Utils::startSession()->checkSession()->restrictMethods();

?>

<div class="container">
  <div class="row pixcrate-rowmt-10">
    <div class="col-12 col-sm-4">

      <div class="card" id="homeCard">
        <a href="<?=ABS_PATH . "home"?>" class="text-center underline-none">
          <div class="card-img-top p-5 pixcrate-std-red">
            <img src="<?=ABS_PATH . CFGList::path_sheet['img_path'] . "icons/home.png"?>" height="96px" class="mx-auto d-block img-white">
          </div>
          <div class="card-body">Home</div>
        </a>
        <div class="card-footer pt-3 pb-1 pixcrate-gray-light-7">
            <p class="text-center">Go to your user screen</p>
        </div>
      </div>

      <div class="card shadow-sm mt-5" id="homeCard">
        <a href="<?=ABS_PATH?>management/repository" class="text-center underline-none">
          <div class="card-img-top p-5 pixcrate-std-red">
            <img src="<?=ABS_PATH . CFGList::path_sheet['img_path'] . "icons/crate.png"?>" height="96px" class="mx-auto d-block img-white">
          </div>
          <div class="card-body">Repository Management</div>
        </a>
        <div class="card-footer pt-3 pb-1 pixcrate-gray-light-7">
            <p class="text-center">Go to the repository management panel</p>
        </div>
      </div>

    </div>
    <div class="col-12 col-sm-4">

      <div class="card shadow-sm">
        <a href="<?=ABS_PATH?>management/user" class="text-center underline-none">
          <div class="card-img-top p-5 pixcrate-std-red">
            <img src="<?=ABS_PATH . CFGList::path_sheet['img_path'] . "icons/user.png"?>" height="96px" class="mx-auto d-block img-white">
          </div>
          <div class="card-body">User Management</div>
        </a>
        <div class="card-footer pt-3 pb-1 pixcrate-gray-light-7">
          <p class="text-center">Go to users management panel</p>
        </div>
      </div>

      <div class="card shadow-sm mt-5">
        <a href="management/image" class="text-center underline-none">
          <div class="card-img-top p-5 pixcrate-std-red">
            <img src="<?=ABS_PATH . CFGList::path_sheet['img_path'] . "icons/image.png"?>" height="96px" class="mx-auto d-block img-white">
          </div>
          <div class="card-body">Image Management</div>
        </a>
        <div class="card-footer pt-3 pb-1 pixcrate-gray-light-7">
          <p class="text-center">Go to images management panel</p>
        </div>
      </div>

    </div>
    <div class="col-12 col-sm-4">

      <div class="card shadow-sm">
        <a href="management/link" class="text-center underline-none">
          <div class="card-img-top p-5 pixcrate-std-red">
            <img src="<?=ABS_PATH . CFGList::path_sheet['img_path'] . "icons/user-repository.png"?>" height="96px" class="mx-auto d-block img-white">
          </div>
          <div class="card-body">Repository-User Linking</div>
        </a>
        <div class="card-footer pt-3 pb-1 pixcrate-gray-light-7">
          <p class="text-center">Go to the user-repository linking panel</p>
        </div>
      </div>

      <div class="card mt-5">
        <a href="api" class="text-center underline-none">
          <div class="card-img-top p-5 pixcrate-std-red">
            <img src="<?=ABS_PATH . CFGList::path_sheet['img_path'] . "icons/api.png"?>" height="96px" class="mx-auto d-block img-white">
          </div>
          <div class="card-body">API</div>
        </a>
        <div class="card-footer pt-3 pb-1 pixcrate-gray-light-7">
          <p class="text-center">Visit the official Pixcrate image API</p>
        </div>
      </div>

    </div>
  </div>

  <div class="row pixcrate-rowmt-5 ">
      <div class="col-12 col-sm-4"></div>
      <div class="col-12 col-sm-4">
        <a href="logout" id="logoutButton" class="round-button pixcrate-std-red">
          <img src="<?=ABS_PATH . CFGList::path_sheet['img_path'] . "icons/shutdown.png"?>" height="32px" class="m-auto d-block img-white">
        </a>
      </div>
      <div class="col-12 col-sm-4"></div>
  </div>
</div>
