<?php
require_once "core/hyppos/user.php";
require_once "core/models/usermodel.php";

if (!isset($_SESSION)) {
    session_start();
}

if (!isset($_SESSION["id"])) {
    header("Location:" . ABS_PATH);
} else if (isset($_SESSION["serveradmin"]) && $_SESSION["serveradmin"] != 1) {
    header("Location:" . ABS_PATH . "home");
}

?>

<head>
  <link rel="stylesheet" href="<?=ABS_PATH . CFGList::path_sheet['style_path']?>management-user.css">
  <script src="<?=ABS_PATH . CFGList::path_sheet['script_path']?>jquery.validate.js"></script>
  <script src="<?=ABS_PATH . CFGList::path_sheet['script_path']?>management/user.js"></script>
  <script src="<?=ABS_PATH . CFGList::path_sheet['script_path']?>jquery-ui-1.12.1.custom/jquery-ui.js"></script>
</head>

<body>

  <!-- *************************** CREATE *************************** -->

  <div id="dialog-form-create" title="Create a new row" style="display:none;">
    <p class="validateTips">All form fields are required.</p>

    <form id="createForm">
      <fieldset>

        <label for="createEmail">Email</label>
        <input type="text" name="createEmail" id="createEmail" class="text ui-widget-content ui-corner-all"> <br>

        <label for="createUsername">Username</label>
        <input type="text" name="createUsername" id="createUsername" class="text ui-widget-content ui-corner-all"> <br>

        <label for="createPassword">Password</label>
        <input type="password" name="createPassword" id="createPassword" class="text ui-widget-content ui-corner-all"> <br>

        <label for="createPasswordConfirm">Confirm Password</label>
        <input type="password" name="createPasswordConfirm" id="createPasswordConfirm" class="text ui-widget-content ui-corner-all"> <br>

        <label for="createName">Name</label>
        <input type="text" name="createName" id="createName" class="text ui-widget-content ui-corner-all"> <br>

        <label for="createSurname">Surname</label>
        <input type="text" name="createSurname" id="createSurname" class="text ui-widget-content ui-corner-all"> <br>

        <!-- <label for="birthDate">Birth Date:</label>
        <input type="text" name="birthDate" id="createBirthDate"> <br> -->

        <label for="createServeradmin">Server Admin</label>
        <select name="createServeradmin" id="createServeradmin">
          <option value="0" >False</option>
          <option value="1" >True</option>
        </select>


        <!-- Allow form submission with keyboard without duplicating the dialog button -->
        <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
      </fieldset>
    </form>
  </div>


  <!-- *************************** UPDATE *************************** -->

  <div id="dialog-form-edit" title="Edit this row" style="display:none;">
    <p class="validateTips">All form fields are required.</p>

    <form >
      <fieldset>

        <label for="username">Username</label>
        <input type="text" name="username" id="editUsername" class="text ui-widget-content ui-corner-all"> <br>

        <label for="name">Name</label>
        <input type="text" name="name" id="editName" class="text ui-widget-content ui-corner-all"> <br>

        <label for="surname">Surname</label>
        <input type="text" name="surname" id="editSurname" class="text ui-widget-content ui-corner-all"> <br>

        <label for="serveradmin">Server Admin</label>
        <select name="serveradmin" id="editServeradmin">
          <option value="0" >False</option>
          <option value="1" >True</option>
        </select>

        <!-- Allow form submission with keyboard without duplicating the dialog button -->
        <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
      </fieldset>
    </form>
  </div>

  <!-- *************************** DELETE *************************** -->

  <div id="dialog-confirm" title="Delete selected" style="display: none;">
    <p><span style="float:left; margin:12px 12px 20px 0;"></span>This row is going to dissapear. Are you sure?</p>
  </div>

  <?php

$search = isset($_GET["search"]) ? $_GET["search"] : null;
$quantity = isset($_GET["quantity"]) ? $_GET["quantity"] : null;
$totalPages = isset($_GET["totalPages"]) ? $_GET["totalPages"] : null;
$orderBy = isset($_GET["orderBy"]) ? $_GET["orderBy"] : null;
$sort = isset($_GET["sort"]) ? $_GET["sort"] : null;

$result = UserModel::loadAnyContaining($search, " LIMIT " . $quantity);
?>
  <!--<p>Pages loaded: <?=$totalPages?></p>-->

  <?php
/* *************************** CREATE *************************** */
$createConfirmation = isset($_POST["createConfirm"]) ? $_POST["createConfirm"] : null;
if ($createConfirmation == 1) {
    $user = new User(array("email" => $_POST["userEmail"], "username" => $_POST["userName"], "password" => password_hash($_POST["userPassword"], PASSWORD_DEFAULT),
        "name" => $_POST["userFirstName"], "surname" => $_POST["userSurname"], "serveradmin" => $_POST["userAdmin"]));
    $result = UserModel::save($user);
}

/* *************************** DELETE *************************** */
$deleteConfirmation = isset($_POST["deleteConfirm"]) ? $_POST["deleteConfirm"] : null;
if ($deleteConfirmation == 1) {
    $user = new User(array("email" => $_POST["userEmail"]));
    UserModel::delete($user);
}

/* *************************** UPDATE *************************** */
$editConfirmation = isset($_POST["editConfirm"]) ? $_POST["editConfirm"] : null;
if ($editConfirmation == 1) {
    $user = new User(array("email" => $_POST["userEmail"], "username" => $_POST["userName"], "password" => password_hash($_POST["userPassword"], PASSWORD_DEFAULT),
        "name" => $_POST["userFirstName"], "surname" => $_POST["userSurname"], "serveradmin" => $_POST["userAdmin"]));
    $result = UserModel::update($user, "WHERE email=?");
}

?>
    <div class="container-fluid pixcrate-rowmt-5">
      <div class="row">
        <div class="col-12 col-sm-2"></div>
        <div class="col-12 col-sm-8" id="tableContainer">
          <table class="table table-striped rounded">
            <thead class="text-center">
              <tr class="pixcrate-red-std">
                <th>#</th>
                <th>E-mail</th>
                <th>Username</th>
                <th>Password</th>
                <th>Name</th>
                <th>Surname</th>
                <th>Admin</th>
                <th>Ban</th>
                <th>Api Key</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
                <?php
                  $i = -1; //FOR KNOWING THE ITERATION NUMBER
                  foreach ($result as $array) {
                    $i++;
                    echo"<tr class='pixcrate-row' id='$i'>" .
                        "<td scope='row'>$i</th>" .
                        "<td class='email'>" . $array["email"] . "</td>" .
                        "<td class='username'>" . $array["username"] . "</td>" .
                        "<td class='password'>" . $array["password"] . "</td>" .
                        "<td class='name'>" . $array["name"] . "</td>" .
                        "<td class='surname'>" . $array["surname"] . "</td>" .
                        "<td class='serveradmin'>" . $array["serveradmin"] . "</td>" .
                        "<td class='ban'>" . $array["ban"] . "</td>" .
                        "<td class='apikey'>" . $array["apikey"] . "</td>" .
                        "<td><button class='editButton default-button'>Edit</button><button class='deleteButton default-button'>Delete</button></td>" .
                        "</tr>";
                  }
                ?>
            </tbody>

          </table>

        </div>
        <div class="col-12 col-sm-2">
          <div class="card">
            <div class="card-header text-white text-center">Control panel</div>
            <div class="card-body pixcrate-red-std">
              <button class="std-button" id="createButton">Add</button>
              <input type="text" name="search" placeholder="Search emails, usernames, names..." class="default-font" id="search">
              <form method="get">
                  <select name="quantity" id="quantity">
                    <option value="0" default>0</option>
                    <option value="5">5</option>
                    <option value="25">25</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                    <option value="200">200</option>
                    <option value="300">300</option>
                  </select>
              </form>
              <form method="get">
                <select name="orderBy" id="orderBy">
                    <option value="" default>Select</option>
                    <option value="email">Email</option>
                    <option value="password">Password</option>
                    <option value="username">Username</option>
                    <option value="name">First Name</option>
                    <option value="surname">Last Name</option>
                    <option value="serveradmin">Rights</option>
                    <option value="ban">Ban</option>
                  </select>
              </form>
              <form method="get">
                <select name="sort" id="sort">
                  <option value="" default>Select</option>
                  <option value="ASC">ASC</option>
                  <option value="DESC">DESC</option>
                </select>
              </form>
            </div>
          </div>
        </div>
      </div>

    </div>


  <!--<table class="table table-striped" id="table">
    <thead>
      <tr id="fields">
        <th>#</th>
        <th>Email</th>
        <th>Username</th>
        <th>Password</th>
        <th>Name</th>
        <th>Surname</th>
        <th>Admin</th>
        <th>Ban</th>
        <th>Api Key</th>
        <th style="display: flex; position: relative; flex-wrap: wrap;" class="default-search-bar">
          <input type="text" name="search" placeholder="Search emails, usernames, names..." class="default-font" id="search">
          <button id="createButton" class="default-button" >Add</button>
          <form method="get">
              <select name="quantity" id="quantity">
                    <option value="0" default>0</option>
                    <option value="5">5</option>
                    <option value="25">25</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                    <option value="200">200</option>
                    <option value="300">300</option>
              </select>
              <select name="orderBy" id="orderBy">
                    <option value="" default>Select</option>
                    <option value="email">Email</option>
                    <option value="password">Password</option>
                    <option value="username">Username</option>
                    <option value="name">First Name</option>
                    <option value="surname">Last Name</option>
                    <option value="serveradmin">Rights</option>
                    <option value="ban">Ban</option>
              </select>
              <select name="sort" id="sort">
                    <option value="" default>Select</option>
                    <option value="ASC">ASC</option>
                    <option value="DESC">DESC</option>
              </select>
          </form>
        </th>
      </tr>
    </thead>

    <tbody id="userList">
      <?php
      /*
        $i = -1; //FOR KNOWING THE ITERATION NUMBER
        foreach ($result as $array) {
            $i++;
            echo "<tr class='row' id='" . $i . "'>" .
                "<th scope='row'>$i</th>" .
                "<td class='email'>" . $array["email"] . "</td>" .
                "<td class='username'>" . $array["username"] . "</td>" .
                "<td class='password'>" . $array["password"] . "</td>" .
                "<td class='name'>" . $array["name"] . "</td>" .
                "<td class='surname'>" . $array["surname"] . "</td>" .
                "<td class='serveradmin'>" . $array["serveradmin"] . "</td>" .
                "<td class='ban'>" . $array["ban"] . "</td>" .
                "<td class='apikey'>" . $array["apikey"] . "</td>" .
                "<td><button class='editButton default-button'>Edit</button><button class='deleteButton default-button'>Delete</button></td>" .
                "</tr>";

        }
        */
      ?>

      <div >

      </div>
    </tbody>


  </table>
  -->

  <!--<div style="margin-left:45%; margin-top:1%;">
    <button id="showMoreButton">Show More</button>
    <button id="showLessButton">Show Less</button>
  </div>-->

</body>