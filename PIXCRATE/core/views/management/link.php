<?php
  require_once("core/hyppos/user.php");
  require_once("core/hyppos/repository.php");
  require_once("core/hyppos/repositoryuser.php");
  require_once("core/models/usermodel.php");
  require_once("core/models/repositorymodel.php");
  require_once("core/models/repositoryusermodel.php"); 

  if(!isset($_SESSION)){
    session_start();
  }

  if(!isset($_SESSION["id"])){
    header("Location:".CFGList::p_absolute."/");
  }

?>

<head>
  <script src="<?=CFGList::p_absolute."/".CFGList::path_sheet['script_path']?>management/link.js"></script>
</head>

<body>

<!-- *************************** DELETE *************************** -->

<div id="dialog-confirm" title="Delete selected" style="display: none;">
  <p><span style="float:left; margin:12px 12px 20px 0;"></span>This row is going to dissapear. Are you sure?</p>
</div>

<?php 
  /* *************************** LOAD AND SHOW *************************** */

  $search= isset($_GET["search"])? $_GET["search"] : null;
  $quantity= isset($_GET["quantity"])? $_GET["quantity"] : null;
  $searchResult= RepositoryModel::loadAnyContaining($search, "LIMIT ".$quantity);
  $users= UserModel::loadAnyContaining("", "");
?>

<?php

  /* *************************** LINK *************************** */
  $linkConfirmation= isset($_POST["linkConfirm"])? $_POST["linkConfirm"] : null;
  if($linkConfirmation == 1) {
    $repositoryUser= new Repository_Has_User(array("repository_repositoryId" => $_POST["repositoryId"], "user_email" => $_POST["user_email"], "permission" => $_POST["permission"]));
    $result= RepositoryUserModel::save($repositoryUser, "");
    print_r($result);
  }

?>

<table class="default-shadow dark-table" style="margin: auto; color: #d5d7d5" id="table">
  <tr id="fields">
    <th>Id</th>
    <th>Name</th>
    <th style="display: flex; position: relative; flex-wrap: wrap;" class="default-search-bar">
      <input type="text" name="search" placeholder="Search emails, usernames, names..." class="default-font" id="search" style="width: 90%;">
      <button id="createButton" class="default-button" >Add</button>
      <form method="get">
          <select name="quantity" id="quantity">
                <option value="0" default>0</option>
                <option value="5">5</option>
                <option value="25">25</option>
                <option value="50">50</option>
                <option value="100">100</option>
                <option value="200">200</option>
                <option value="300">300</option>
          </select>
          <select name="orderBy" id="orderBy">
                <option value="" default>Select</option>
                <option value="email">Email</option>
                <option value="password">Password</option>
                <option value="username">Username</option>
                <option value="name">First Name</option>
                <option value="surname">Last Name</option>
                <option value="serveradmin">Rights</option>
                <option value="ban">Ban</option>
          </select>
          <select name="sort" id="sort">
                <option value="" default>Select</option>
                <option value="ASC">ASC</option>
                <option value="DESC">DESC</option>
          </select>
      </form>
    </th>
  </tr>

  <?php
    $i= -1; //FOR KNOWING THE ITERATION NUMBER
    foreach($searchResult as $array){
      $i++; 
      echo "<tr class='row' style='color: #3fd300; overflow: hidden;' id='".$i."'>".
      "<td class='repositoryId'>".$array["repositoryId"]."</td>".
      "<td class='name'>".$array["name"]."</td>".
      "<td><select name='userSelect' id='userSelect'>";
      foreach($users as $array){
        echo "<option value=".$array["email"].">".$array["username"]."</option>";
      }
      echo "</select>";
      echo 
      "<select name='permission' class='permission'>".
      "<option value='null'>Select</option>".
      "<option value='OWNER'>OWNER</option>".
      "<option value='ADMIN'>ADMIN</option>".
      "<option value='RW'>RW</option>".
      "<option value='R'>R</option>".
      "<option value='BLOCKED'>BLOCKED</option>".
      "</select>".
      "<button class='linkButton'>Link</button></td>";
    }

  ?>

  </tr>
  
</table>

</body>