<?php
  require_once("core/hyppos/repository.php");
  require_once("core/hyppos/repositoryuser.php");
 
  require_once("core/hyppos/user.php");
  require_once("core/models/usermodel.php");
  require_once("core/models/repositoryusermodel.php");

  require_once("core/models/repositorymodel.php");
  require_once("core/models/repositoryusermodel.php");
  require_once("core/utils.php");

  if(!isset($_SESSION)){
    session_start();
  }

  if(!isset($_SESSION["id"])){
    header("Location:".CFGList::p_absolute."/");
  } else if(isset($_SESSION["serveradmin"]) && $_SESSION["serveradmin"] != 1){
    header("Location:".CFGList::p_absolute."/home");
  }

?>

<head>
  <script src="<?=CFGList::p_absolute."/".CFGList::path_sheet['script_path']?>management/repository.js"></script>
  <script src="<?=CFGList::p_absolute."/".CFGList::path_sheet['script_path']?>jquery-ui-1.12.1.custom/jquery-ui.js"></script>
</head>

<body>

  <!-- *************************** CREATE *************************** -->

  <div id="dialog-form-create" title="Create a new row" style="display:none;">
    <p class="validateTips">All form fields are required.</p>
  
    <form >
      <fieldset>

        <label for="repositoryName">Name</label>
        <input type="text" name="repositoryName" id="createRepositoryName" class="text ui-widget-content ui-corner-all"> <br>

        <label for="description">Description</label>
        <input type="text" name="description" id="createDescription" class="text ui-widget-content ui-corner-all"> <br>

        <label for="repositoryVisible">Visible</label>
        <select name="repositoryVisible" id="createRepositoryVisible">
          <option value="PRIVATE">PRIVATE</option>
          <option value="HIDDEN">HIDDEN</option>
          <option value="PUBLIC">PUBLIC</option>
        </select>
      
        <!-- Allow form submission with keyboard without duplicating the dialog button -->
        <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
      </fieldset>
    </form>
  </div>


  <!-- *************************** UPDATE *************************** -->

  <div id="dialog-form-edit" title="Edit this row" style="display:none;">
    <p class="validateTips">All form fields are required.</p>
  
    <form>
      <fieldset>

        <label for="repositoryName">Name</label>
        <input type="text" name="repositoryName" id="editRepositoryName" class="text ui-widget-content ui-corner-all"> <br>

        <label for="description">Description</label>
        <input type="text" name="description" id="editDescription" class="text ui-widget-content ui-corner-all"> <br>

        <label for="repositoryVisible">Visible</label>
        <select name="repositoryVisible" id="editRepositoryVisible">
          <option value="PRIVATE">PRIVATE</option>
          <option value="HIDDEN">HIDDEN</option>
          <option value="PUBLIC">PUBLIC</option>
        </select>
      
        <!-- Allow form submission with keyboard without duplicating the dialog button -->
        <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
      </fieldset>
    </form>
  </div>

  <!-- *************************** DELETE *************************** -->

  <div id="dialog-confirm" title="Delete selected" style="display: none;">
    <p><span style="float:left; margin:12px 12px 20px 0;"></span>This row is going to dissapear. Are you sure?</p>
  </div>


  <?php 
    /* *************************** LOAD AND SHOW *************************** */

    $search= isset($_GET["search"])? $_GET["search"] : null;
    $quantity= isset($_GET["quantity"])? $_GET["quantity"] : null;
    $result= RepositoryModel::loadAnyContaining($search, "LIMIT ".$quantity);
  ?>

  <?php

    /* *************************** CREATE *************************** */
    $createConfirmation= isset($_POST["createConfirm"])? $_POST["createConfirm"] : null;
    if($createConfirmation == 1) {
      $repository= new Repository(array("repositoryId" => Utils::generateId(rand(1, 45)), "name" => $_POST["repositoryName"], "visible" => $_POST["repositoryVisible"], 
      "description" => $_POST["description"]));
      $result= RepositoryModel::save($repository);

    }

    /* *************************** DELETE *************************** */
    $deleteConfirmation= isset($_POST["deleteConfirm"])? $_POST["deleteConfirm"] : null;
    if($deleteConfirmation == 1) {
      $repository= new Repository(array("repositoryId" => $_POST["repositoryId"]));
      RepositoryModel::delete($repository);
    }

    /* *************************** UPDATE *************************** */
    $editConfirmation= isset($_POST["editConfirm"])? $_POST["editConfirm"] : null;
    if($editConfirmation == 1) {
      $repository= new Repository(array("repositoryId" => $_POST["repositoryId"], "name" => $_POST["repositoryName"], "visible" => $_POST["repositoryVisible"], 
      "description" => $_POST["description"]));
      $result= RepositoryModel::update($repository, "WHERE repositoryId=?");
      print_r($_POST["repositoryId"]);
    }

    /* *************************** LINK *************************** */
    $linkConfirmation= isset($_POST["linkConfirm"])? $_POST["linkConfirm"] : null;
    if($linkConfirmation == 1) {
      $repositoryUser= new RepositoryUser(array("repository_repositoryId" => $_POST["repositoryId"], "user_email" => $_POST["user_email"], "permission" => $_POST["permission"]));
      $result= RepositoryUserModel::save($repositoryUser, "");
    }

  ?>
  <table class="default-shadow dark-table" style="margin: auto; color: #d5d7d5" id="table">
    <tr id="fields">
      <th>Id</th>
      <th>Name</th>
      <th>Visible</th>
      <th>Description</th>
      <th style="display: flex; position: relative; flex-wrap: wrap;" class="default-search-bar">
        <input type="text" name="search" placeholder="Search emails, usernames, names..." class="default-font" id="search" style="width: 90%;">
        <button id="createButton" class="default-button" >Add</button>
        <form method="get">
            <select name="quantity" id="quantity">
                  <option value="0" default>0</option>
                  <option value="5">5</option>
                  <option value="25">25</option>
                  <option value="50">50</option>
                  <option value="100">100</option>
                  <option value="200">200</option>
                  <option value="300">300</option>
            </select>
            <select name="orderBy" id="orderBy">
                  <option value="" default>Select</option>
                  <option value="email">Email</option>
                  <option value="password">Password</option>
                  <option value="username">Username</option>
                  <option value="name">First Name</option>
                  <option value="surname">Last Name</option>
                  <option value="serveradmin">Rights</option>
                  <option value="ban">Ban</option>
            </select>
            <select name="sort" id="sort">
                  <option value="" default>Select</option>
                  <option value="ASC">ASC</option>
                  <option value="DESC">DESC</option>
            </select>
        </form>
      </th>
    </tr>
    <?php
      $i= -1; //FOR KNOWING THE ITERATION NUMBER
      foreach($result as $array){
        $i++; 
        echo "<tr class='row' style='color: #3fd300; overflow: hidden;' id='".$i."'>".
        "<td class='repositoryId'>".$array["repositoryId"]."</td>".
        "<td class='name'>".$array["name"]."</td>".
        "<td class='visible'>".$array["visible"]."</td>".
        "<td class='description'>".$array["description"]."</td>".
        "<td><button class='editButton'>Edit</button><button class='deleteButton'>Delete</button></td>".
        "</tr>";
        
      }

    ?>
    
  </table>
  
</body>