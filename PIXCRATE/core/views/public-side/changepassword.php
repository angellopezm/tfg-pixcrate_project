<head>
    <script src="<?=ABS_PATH . CFGList::path_sheet['script_path']?>jquery-ui-1.12.1.custom/jquery-ui.js"></script>
    <script src="<?=ABS_PATH . CFGList::path_sheet['script_path']?>jquery.validate.js"></script>
    <script src="<?=ABS_PATH . CFGList::path_sheet['script_path']?>additional-methods.js"></script>
    <script src="<?=ABS_PATH . CFGList::path_sheet['script_path']?>main/changePassword.js"></script>
</head>

<?php
require_once "core/hyppos/user.php";
require_once "core/models/usermodel.php";

$email= isset($_GET["email"])? $_GET["email"]: null;
$hash= isset($_GET["hash"])? $_GET["hash"]: null;
?>

<div class="row no-gutters">
    <div class="col-12 col-sm-2 pixcrate-std-red"></div>
    <div class="col-12 col-sm-8">
        <nav class="pixcrate-std-red navbar">
            <span class="navbar-brand text-white text-lg">Pixcrate</span>
        </nav>
    </div>
    <div class="col-12 col-sm-2 pixcrate-std-red"></div>
</div>
<div class="row no-gutters m-5">
    <div class="col-12 col-sm-12"></div>
</div>

<div class="row no-gutters">
    <div class="col-12 col-sm-2 col-lg-4"></div>
    <div class="col-12 col-sm-8 col-lg-4">
        <div class="card mt-5 p-5 pixcrate-text-dark">
            <form method="post" id="regForm">
                <div class="form-group">
                    <label for="password" class="d-block">New Password</label>
                    <input placeholder="Password" name="password" id="password" type="password" class="validate form-control input-has-border">
                </div>
                <div class="form-group">
                    <label for="passwordConfirm" class="d-block">Confirm Password</label>
                    <input placeholder="Password" name="passwordConfirm" id="passwordConfirm" type="password" class="validate form-control input-has-border">
                </div>
                <div class="d-flex mt-3">
                    <input class="btn std-button px-4 py-2 pixcrate-std-red value-text-white" type="submit" value="Confirm">
                </div>
            </form>
        </div>
    </div>
    <div class="col-12 col-sm-2 col-lg-4"></div>
</div>

<?php

$password = isset($_POST["password"]) ? password_hash($_POST["password"], PASSWORD_DEFAULT) : null;

if((isset($email) && $email != "") && (isset($hash) && $hash != "") && isset($password)) {
    $user = new User([
      "email" => $email
    ]);
    $result = UserModel::load($user);
    if(sizeof($result) > 0 && (isset($result["hash"]) && $result["hash"] != "") ) {
        if($hash == $result["hash"]) {
            $user = new User([
                "email" => $email,
                "password" => $password
            ]);
            UserModel::update($user, " WHERE email=?");
            $result = UserModel::load($user);
            if(sizeof($result) > 0) {
                $user = new User([
                    "email" => $email,
                    "hash" => " "
                ]);
                UserModel::update($user, " WHERE email=?");
                echo "<h1 class='text-center mx-auto mt-4'>Password changed correctly. You can close this now.</h1>";
            }
        } else {
            echo "<h1 class='text-center mx-auto mt-4'>Error: Something went wrong when updating your password. Try again or contact the supports.</h1>";
        }
    }
}

?>