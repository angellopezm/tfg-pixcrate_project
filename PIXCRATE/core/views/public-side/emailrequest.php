<?php
require_once "core/utils.php";

require_once "core/hyppos/user.php";
require_once "core/models/usermodel.php";

$hash = isset($_GET["hash"]) ? $_GET["hash"] : null;
?>

<div class="row no-gutters">
    <div class="col-12 col-sm-2 pixcrate-std-red"></div>
    <div class="col-12 col-sm-8">
        <nav class="pixcrate-std-red navbar">
            <span class="navbar-brand text-white text-lg">Pixcrate</span>
        </nav>
    </div>
    <div class="col-12 col-sm-2 pixcrate-std-red"></div>
</div>
<div class="row no-gutters m-5">
    <div class="col-12 col-sm-12"></div>
</div>

<div class="row no-gutters">
    <div class="col-12 col-sm-2 col-lg-4"></div>
    <div class="col-12 col-sm-8 col-lg-4">
        <div class="card mt-5 p-5 pixcrate-text-dark">
            <form method="post">
                <div class="form-group">
                    <label for="name" class="d-block">Email verification</label>
                    <span class="text-muted d-block mb-3 font-size-8">Introduce your email here, then confirm. We will send you an email to this adress in order to confirm your identity.</span>
                    <input placeholder="Email" name="email" type="email" class="validate form-control input-has-border">
                </div>
                <div class="d-flex mt-3">
                    <input class="btn std-button px-4 py-2 pixcrate-std-red value-text-white" type="submit" value="Confirm">
                </div>
            </form>
            <div class="mt-4">
                <span><a href="javascript:history.back()">Back «</a></span>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-2 col-lg-4"></div>
</div>

<?php

$email = isset($_POST["email"]) ? $_POST["email"] : null;

$user = new User([
    "email" => $email
]);
$result = UserModel::load($user);
if($result["verified"]) {
    $hash = md5(rand(0,1000));
    $user = new User([
        "email" => $email,
        "hash" => $hash
    ]);
    UserModel::update($user, "WHERE email=?");

    Utils::mail($email, "Identity verification. Password change request", 
        "Someone is trying to change your password. If it's you, this is the link for changing your password. Please don't reply to this, because it will not be replied anyway.\n\n" . 
        ABS_PATH . "password/change?email=" . $email . "&hash=" . $hash
    );
    echo "Email sent";
} else {
    if(isset($email)) {
        die("This user email doesn't exist on Pixcrate or isn't verified yet. If you are, contact Pixcrate's support.");
    }
}
?>