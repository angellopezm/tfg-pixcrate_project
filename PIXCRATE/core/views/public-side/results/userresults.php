﻿<?php 
if(sizeof($resultUsers) != 0) {
    $i= 1;
    foreach($resultUsers as $row) {
?>
<div class="row no-gutters mx-3">
    <a href=<?=ABS_PATH . "explore/users?name=" . $row["username"]?> class="col-12 col-xl-11 col-lg-10 col-md-9 py-3 pl-4 rounded-0 pixcrate-text-dark d-flex underline-none border bg-white">
        <div class="border rounded overflow-hide pixcrate-border-dark pixcrate-gray-light-10" width=90px height=90px >
            <img src="<?=$row["profileImage"]?>" width=90px height=90px >
        </div>
        <div class="ml-4 mt-4 pr-3">
            <p class="center-align pixcrate-text-dark font-weight-bold h5 m-0">
                <?=$row["username"]?>
                <i class="material-icons d-inline align-middle text-muted">person</i>
            </p>
            <span class="center-align font-size-8 text-muted text-truncate">
                <?=$row["email"]?>
            </span>
        </div>
    </a>
</div>
<?php
    }
} else {
?>
    <div class="w-100 text-center">
        <h1>There's no results for users</h1>
        <p class="text-muted">Try typing another word in the search bar above. Good Luck!</p>
    </div>
<?php
}
?>