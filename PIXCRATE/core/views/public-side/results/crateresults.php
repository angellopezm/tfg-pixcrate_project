﻿<?php 
if(sizeof($resultCrates) != 0) {
    $i= 1;
    foreach($resultCrates as $row) {
        $link = new Repository_Has_User(array("repository_repositoryId" => $row["repositoryId"]));
        $linkedCrate = RepositoryUserModel::load($link);

        if($linkedCrate["permission"] == "OWNER") {
            $visibility = "";
        } else {
            $visibility = "PUBLIC";
        }

        $image = new Image([
            "repository_repositoryId" => $row["repositoryId"],
            "visibility" => $visibility
        ]);
        $allImagesFromRep = ImageModel::loadAll($image, " LIMIT 4");
?>
<div class="container-fluid">
        <div class="row no-gutters">
            <a href=<?=ABS_PATH . "explore/crates?id=" . $row["repositoryId"]?> class="col-12 col-xl-11 col-lg-10 col-md-9 py-3 pl-4 rounded-0 pixcrate-text-dark d-flex underline-none border bg-white">
                <div class="border rounded overflow-hide pixcrate-border-dark pixcrate-gray-light-10" style="width: 180px; height: 90px;">
                    <?php
                        if(sizeof($allImagesFromRep) > 1) {
                    ?>
                    <div class="carousel slide" data-ride="carousel" data-interval="5000">
                        <div class="carousel-inner">
                            <?php
                                $i = 0;
                                foreach($allImagesFromRep as $image) {
                            ?>  
                            <div class="carousel-item <?php if($i == 0 ) echo " active";?>">
                                <img src="<?=$allImagesFromRep[$i]["url"]?>" width=180px class="pixcrate-image-blur">
                            </div>
                            <?php   
                                    $i++;
                                }
                            ?>
                        </div>
                    </div>
                    <?php
                        } else if(sizeof($allImagesFromRep) == 1){
                    ?>
                    <img src="<?=$allImagesFromRep[0]["url"]?>" width=320px class="pixcrate-image-blur">
                    <?php
                        } else {
                    ?>
                    <div class="m-auto text-center">
                        <span class="align-middle">
                            No images found
                        </span>
                    </div>
                    <?php
                        }
                    ?>
                </div>
                <div class="ml-4 mt-4 pr-3">
                    <p class="center-align pixcrate-text-dark font-weight-bold h5 m-0">
                        <?=$row["name"]?>
                        <i class="material-icons d-inline align-middle text-muted">inbox</i>
                    </p>
                    
                    <p class="center-align pixcrate-text-dark m-0 text-muted pixcrate-text-wrap">
                        <?=$row["description"]?>
                    </p>
                </div>
            </a>
        </div>
</div>
<?php
    }
} else {
?>
    <div class="w-100 text-center">
        <h1>There's no results for crates</h1>
        <p class="text-muted">Try typing another word in the search bar above. Good Luck!</p>
    </div>
<?php
}
?>