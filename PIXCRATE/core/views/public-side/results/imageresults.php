﻿<?php 
if(sizeof($resultImages) != 0) {
    $i= 1;
    foreach($resultImages as $row) {
?>
<div class="container-fluid">
        <div class="row no-gutters">
            <a href=<?=ABS_PATH . "explore/images?id=" . $row["imageId"]?> class="col-12 col-xl-11 col-lg-10 col-md-9 py-3 pl-4 rounded-0 pixcrate-text-dark d-flex underline-none border bg-white">
                <div class="border rounded overflow-hide pixcrate-border-dark pixcrate-gray-light-10" style="width: 180px; height: 90px;">
                    <img src="<?=$row["url"]?>" width=180px class="pixcrate-image-blur">
                </div>
                <div class="ml-4 mt-4 pr-3">
                    <p class="center-align pixcrate-text-dark font-weight-bold h5 m-0">
                        <?=$row["title"]?>
                        <i class="material-icons d-inline align-middle text-muted">photo</i>
                    </p>
                    
                    <p class="center-align pixcrate-text-dark m-0 text-muted pixcrate-text-overflow">
                        <?=$row["description"]?>
                    </p>
                </div>
            </a>
        </div>
</div>
<?php
    }
} else {
?>
    <div class="w-100 text-center">
        <h1>There's no results for images</h1>
        <p class="text-muted">Try typing another word in the search bar above. Good Luck!</p>
    </div>
<?php
}
?>