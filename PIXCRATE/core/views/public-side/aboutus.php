<div class="row no-gutters">
    <div class="col-12 col-sm-2 pixcrate-std-red"></div>
    <div class="col-12 col-sm-8">
        <nav class="pixcrate-std-red navbar">
            <span class="navbar-brand text-white text-lg">Pixcrate</span>
        </nav>
    </div>
    <div class="col-12 col-sm-2 pixcrate-std-red"></div>
</div>
<div class="row no-gutters m-5">
    <div class="col-12 col-sm-12"></div>
</div>

<div class="row no-gutters p-5">
    <div class="text-center m-auto">
        <h1 class="font-weight-bold">What is Pixcrate?</h1>
        <p class="text-muted mt-5">
            Pixcrate wants to be a website for artists and people who just wants to share their pictures, drawings, paintings...etc. Additionally, we want to encourage design teams to store here their designs and benefit them by giving them visibility.
        </p>
        <p class="text-muted">
            We know it’s hard to get a job being an artist, that’s why we want to make this your place.
        </p>
    </div>

</div>

<div class="row no-gutters p-5">
    <div class="text-center m-auto">
        <h1 class="font-weight-bold">Conditions</h1>
        <p class="text-muted mt-5">
            Everyone can use Pixcrate without any other special conditions. You just need to register and verify your identity. That’s all.
        </p>
        <h2 class="font-weight-bold font-italic mt-5">
            You just need to register and verify your identity. That’s all you need to do.
        </h2>
        <p class="text-muted mt-5">
            Every image belongs to its owner. Only you, the owner of the repository (or an authorized user) are able to delete images. Every single image (only if you are the owner of the repository) is yours, so are repositories (when created by you).
        </p>
        <h2 class="font-weight-bold font-italic mt-5">
            Every single image inside your repository is yours.
        </h2>
        <p class="text-muted mt-5">
            But, what do we store? We only store your email, and optional information like your name, surname or birth date. In addition we store image information like title, description, extension, programmed publication date-time...etc.
        </p>
        <p class="text-muted">
            We will never store the pictures, only if you authorize us to do so. This will be asked for security backups, for example. You will be able to configure this on your main profile view.
        </p>
        <h2 class="font-weight-bold font-italic mt-5">
            We will never store the pictures, only if you authorize us to do so.
        </h2>
    </div>
</div>

<div class="row no-gutters p-5 text-center">
    <a href=<?=ABS_PATH?> class="m-auto"><h3>Back</h3></a>
</div>