<?php
require_once "core/utils.php";

require_once "core/hyppos/image.php";
require_once "core/models/imagemodel.php";

Utils::startSession()->checkSession();
$imageId= isset($_GET["id"])? $_GET["id"]: null;
if(isset($imageId)) {
    $image = new Image(["imageId" => $imageId]);
    $currentImage = ImageModel::load($image);
    if(sizeof($currentImage) < 1){
        header("Location:" . ABS_PATH . "missing/404image");
    } else { 
?>
<div class="row no-gutters pixcrate-std-red">
    <div class="col-12 col-sm-2">
        <nav class="navbar w-100">
            <span class="navbar-brand text-white text-lg m-auto">Pixcrate</span>
        </nav>
    </div>
    <div class="col-12 col-sm-8">
        <nav class="navbar">
            <form class="form-inline d-flex flex-nowrap h-75 ml-auto mt-1" method="get" action=<?=ABS_PATH . "explore"?>>
                <input class="form-control rounded-0 pixcrate-rounded-left border-0 box-shadow-none text-truncate" type="search" placeholder="Search" 
                aria-label="Search" name="search" pattern="[A-Za-z0-9]{1,}" title="One character" required>
                <button class="my-2 my-sm-0 rounded-0 border-0 pixcrate-search-btn bg-white pixcrate-rounded-right" type="submit">
                    <i class="material-icons d-block mt-1 md-24">search</i>
                </button>
            </form>
        </nav>
    </div>
    <div class="col-12 col-sm-2"></div>
</div>
<div class="row no-gutters pixcrate-gray-light-20">
    <div class="col-12 col-sm-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb background-transparent my-auto ml-5">
                <li class="breadcrumb-item"><a href=<?=ABS_PATH . "home"?> class="underline-none pixcrate-text-dark">Home</a></li>
                <li class="breadcrumb-item active " aria-current="page"><?=$currentImage["title"]?></li>
            </ol>
        </nav>
    </div>
</div>
<div class="row no-gutters">
    <div class="col-12 col-sm-2"></div>
    <div class="col-12 col-sm-8">
        <div class="card mx-auto my-5 underline-none pixcrate-border-3 rounded-top w-75 h-75">
            <div class="card-header pixcrate-std-red border-0">
                <h5 class="text-center text-white font-weight-normal m-0"><?=$currentImage["title"]?></h5>
            </div>
            <div class="overflow-hide">
                <img src="<?=$currentImage["url"]?>" class="card-img-top img-fluid rounded-0">
            </div>
            <div class="card-body pixcrate-gray-light-6 border-bottom px-4">
                <p class="center-align"><?=$currentImage["description"]?></p>
            </div>
            <div class="card-footer pixcrate-gray-light-8 border-0 pixcrate-border-3 d-flex flex-wrap w-100">
                <div class="d-flex mr-2">
                    <button class="btn pixcrate-pane-button background-transparent pixcrate-text-dark mx-1 p-1">
                        <span class="text-center text-truncate d-inline align-middle">
                            <?=$currentImage["likes"]?>
                        </span>
                        <i class="material-icons align-middle">thumb_up</i>
                    </button>
                </div>
                <div class="d-flex">
                    <button class="btn pixcrate-pane-button background-transparent pixcrate-text-dark mx-1 p-1">
                        <span class="text-center text-truncate d-inline center-align align-middle">
                            <?=$currentImage["reposts"]?>
                        </span>
                        <i class="material-icons d-inline align-middle">repeat</i>
                    </button>
                </div>
                <div class="background-transparent mx-1 p-1">      
                    <span class="text-center text-truncate d-inline center-align align-middle pixcrate-text-dark">
                        <?=$currentImage["date"]?>
                    </span>
                </div>
                <div class="p-1 text-white">      
                    <span class="text-center text-truncate d-inline center-align align-middle badge badge-secondary pixcrate-std-red">
                        <?=$currentImage["visibility"]?>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-2"></div>
</div>
<?php 
    }
} else {

}
?>