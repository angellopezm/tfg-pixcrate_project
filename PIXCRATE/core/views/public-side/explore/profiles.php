<head>
    <script src="<?=ABS_PATH . CFGList::path_sheet['script_path']?>main/profile.js"></script>
</head>

<?php 
require_once "core/utils.php";

require_once "core/models/usermodel.php";
require_once "core/hyppos/user.php";

require_once "core/hyppos/repository.php";
require_once "core/models/repositorymodel.php";

require_once "core/hyppos/repositoryuser.php";
require_once "core/models/repositoryusermodel.php";

require_once "core/hyppos/image.php";
require_once "core/models/imagemodel.php";


Utils::startSession()->checkSession();

$username = isset($_GET["name"])? $_GET["name"]: null;

$user = new User(array("username" => $username));
$loadedUser = UserModel::load($user);
$profileImage = $loadedUser["profileImage"];

$link = new Repository_Has_User(array("user_email" => $loadedUser["email"]));
$linkedCrates = RepositoryUserModel::loadAll($link);

$totalImages = 0;
$totalOwnedImages = 0;

foreach ($linkedCrates as $row) {
    $image = new Image([
        "repository_repositoryId" => $row["repository_repositoryId"],
    ]);

    if($row["permission"] == "OWNER") {
        $allImagesFromRep = ImageModel::loadAll($image);
        $totalOwnedImages+= sizeof($allImagesFromRep);
    } 

    $allImagesFromRep = ImageModel::loadAll($image);
    $totalImages+= sizeof($allImagesFromRep);
}

?>

<div class="row no-gutters pixcrate-std-red">
    <div class="col-12 col-sm-2">
        <nav class="navbar w-100">
            <span class="navbar-brand text-white text-lg m-auto">Pixcrate</span>
        </nav>
    </div>
    <div class="col-12 col-sm-8">
        <nav class="navbar">
            <form class="form-inline d-flex flex-nowrap h-75 ml-auto mt-1" method="get" action=<?=ABS_PATH . "explore"?>>
                <input class="form-control rounded-0 pixcrate-rounded-left border-0 box-shadow-none text-truncate" type="search" placeholder="Search" 
                aria-label="Search" name="search" pattern="[A-Za-z0-9]{1,}" title="One character" required>
                <button class="my-2 my-sm-0 rounded-0 border-0 pixcrate-search-btn bg-white pixcrate-rounded-right" type="submit">
                    <i class="material-icons d-block mt-1 md-24">search</i>
                </button>
            </form>
        </nav>
    </div>
    <div class="col-12 col-sm-2"></div>
</div>
<div class="row no-gutters pixcrate-gray-light-20">
    <div class="col-12 col-sm-4">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb background-transparent my-auto ml-5">
                <li class="breadcrumb-item"><a href=<?=ABS_PATH . "home"?> class="underline-none pixcrate-text-dark">Home</a></li>
                <li class="breadcrumb-item active " aria-current="page">Profile</li>
            </ol>
        </nav>
    </div>
    <div class="col-12 col-sm-4">
    </div>
    <div class="col-12 col-sm-4"></div>
</div>
<div class="row no-gutters pixcrate-gray-light-8 d-flex">
    <div class="m-auto">
        <div class="mt-5 ml-6 overflow-hide">
            <?php
                if(isset($profileImage) && $profileImage != ""){
            ?>
                <img src=<?=$profileImage?> alt="profile-image" class="pixcrate-circle d-inline-block" width=200 height=200>
            <?php
                }
            ?>
        </div>
        <div class="card background-transparent border-0 mt-3 mb-3 ml-5">
            <div class="card-body">
                <h2 class="font-weight-bold pixcrate-text-dark"><?=$loadedUser["username"]?></h2>
                <span class="card-title text-muted"><?=$loadedUser["email"]?></span>
                <h5 class="card-subtitle mb-2 text-muted"><?=$loadedUser["name"]?> <?=$loadedUser["surname"]?></h6>
                <h6 class="card-subtitle mb-2 text-muted font-size-8">Birth date: <?=date("d-m-Y", strtotime($loadedUser["birth"]))?></h6>
            </div>
        </div>
    </div>
</div>

<div class="row no-gutters pixcrate-gray-light-4 pixcrate-text-dark d-flex">
    <div class="text-center py-5 px-5 mx-auto">
        <i class="material-icons">inbox</i>
        <h1 class="font-weight-bold m-0 d-inline">
            <?=sizeof($linkedCrates)?>
        </h1>
        <span class="d-inline h3">links</span>
        <p class="text-muted">with crates overall</p>
    </div>
    <div class="text-center py-5 px-5 mx-auto">
        <i class="material-icons">photo</i>
        <h1 class="font-weight-bold m-0 d-inline">
            <?=$totalImages?>
        </h1>
        <span class="d-inline h3">links</span>
        <p class="text-muted">with images overall</p>
    </div>
    <div class="text-center py-5 px-5 mx-auto">
        <i class="material-icons">photo</i>
        <h1 class="font-weight-bold m-0 d-inline">
            <?=$totalOwnedImages?>
        </h1>
        <span class="d-inline h3">images</span>
        <p class="text-muted">owned overall</p>
    </div>
    <div class=""></div>
</div>