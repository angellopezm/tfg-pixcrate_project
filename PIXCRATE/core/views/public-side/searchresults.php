<?php
require_once "core/utils.php";

require_once "core/models/usermodel.php";
require_once "core/hyppos/user.php";

require_once "core/hyppos/repository.php";
require_once "core/models/repositorymodel.php";

require_once "core/hyppos/repositoryuser.php";
require_once "core/models/repositoryusermodel.php";

require_once "core/hyppos/image.php";
require_once "core/models/imagemodel.php";

$search = isset($_GET["search"]) ? $_GET["search"] : null;
if(isset($search)) {
    $obj = new User([
        "ban" => "false"
    ]);
    $resultUsers = UserModel::loadAll($obj, "AND username LIKE " . "'%".$search."%'");
    $obj = new Repository([
        "visible" => "PUBLIC"
    ]);
    $resultCrates = RepositoryModel::loadAll($obj, "AND name LIKE " . "'%".$search."%'");
    $obj = new Image([
        "visibility" => "PUBLIC"
    ]);
    $resultImages = ImageModel::loadAll($obj, "AND title LIKE " . "'%".$search."%'");
?>

<div class="row no-gutters pixcrate-std-red">
    <div class="col-12 col-sm-2">
        <nav class="navbar w-100">
            <span class="navbar-brand text-white text-lg m-auto">Pixcrate</span>
        </nav>
    </div>
    <div class="col-12 col-sm-8">
        <nav class="navbar">
            <form class="form-inline d-flex flex-nowrap h-75 ml-auto mt-1" method="get" action=<?=ABS_PATH . "explore"?>>
                <input class="form-control rounded-0 pixcrate-rounded-left border-0 box-shadow-none text-truncate" type="search" placeholder="Search" 
                aria-label="Search" name="search" pattern="[A-Za-z0-9]{1,}" title="One character" required>
                <button class="my-2 my-sm-0 rounded-0 border-0 pixcrate-search-btn bg-white pixcrate-rounded-right" type="submit">
                    <i class="material-icons d-block mt-1 md-24">search</i>
                </button>
            </form>
        </nav>
    </div>
    <div class="col-12 col-sm-2"></div>
</div>

<div class="row no-gutters pixcrate-gray-light-20">
    <div class="col-12 col-sm-4">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb background-transparent my-auto ml-5">
                <li class="breadcrumb-item"><a href=<?=ABS_PATH . "home"?> class="underline-none pixcrate-text-dark">Home</a></li>
                <li class="breadcrumb-item active " aria-current="page">Explore</li>
            </ol>
        </nav>
    </div>
    <div class="col-12 col-sm-4">
    </div>
    <div class="col-12 col-sm-4"></div>
</div>

<div class="row no-gutters mt-5">
    <div class="col-12 col-xl-3 col-lg-4 col-sm-6 p-3 border-right">
        <div class="list-group mx-auto mt-3 pixcrate-text-dark">
            <a href=<?=ABS_PATH . "explore?search=" . $search . "&type=users"?> class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
                Users
                <span class="badge badge-primary badge-pill pixcrate-std-red"><?=sizeof($resultUsers)?></span>
            </a>
            <a href=<?=ABS_PATH . "explore?search=" . $search . "&type=crates"?> class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
                Crates
                <span class="badge badge-primary badge-pill pixcrate-std-red"><?=sizeof($resultCrates)?></span>
            </a>
            <a href=<?=ABS_PATH . "explore?search=" . $search . "&type=images"?> class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
                Images
                <span class="badge badge-primary badge-pill pixcrate-std-red"><?=sizeof($resultImages)?></span>
            </a>
        </div>
    </div>
    <div class="col-12 col-xl-9 col-lg-8 col-sm-6">
        <?php
            if(isset($search)) {
                if(isset($_GET["type"]) && $_GET["type"]== "users") {
                    include_once "results/userresults.php";

                } else if (isset($_GET["type"]) && $_GET["type"]== "crates") {
                    include_once "results/crateresults.php";

                } else if (isset($_GET["type"]) && $_GET["type"]== "images") {
                    include_once "results/imageresults.php";

                } else {
                    include_once "results/userresults.php";
                    include_once "results/crateresults.php";
                    include_once "results/imageresults.php";
                }
            }
        ?>

    </div>
</div>
<?php
}
?>