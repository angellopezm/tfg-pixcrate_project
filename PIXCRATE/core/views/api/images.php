<?php
  require_once "core/hyppos/image.php";
  require_once "core/models/imagemodel.php";
  require_once "core/utils.php";

  $imageId= isset($_GET["imageId"])? $_GET["imageId"]: null;
  $from= isset($_GET["from"])? $_GET["from"]: null;
  $to= isset($_GET["to"])? $_GET["to"]: null;
  $image= null;

  header("Content-Type: application/json");

  if(isset($imageId)){
    $image= new Image(array("imageId" => $imageId));
    $result= ImageModel::load($image, "LIMIT 1");
    
    echo json_encode($result);
  } 
  else{
    $image= new Image(array("visibility" => "PUBLIC"));
    $result= ImageModel::load($image);
    echo json_encode($result);
  }

  
?>