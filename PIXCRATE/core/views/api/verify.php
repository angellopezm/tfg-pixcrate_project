<?php 

  require_once "core/utils.php";
  require_once "core/hyppos/user.php";
  require_once "core/models/usermodel.php";

  Utils::startSession();

  if(isset($_SESSION["apikey"])){
    header("Location:". ABS_PATH ."api/mainpanel");
  }

?>

<form action="" method="post" class="default-search-bar">
  <input type="text" name="apikey" id="apikey" class="default-font" placeholder="Introduce your api key here">
  <input type="submit" value="Check">
</form>

<?php
  $apikey= isset($_POST["apikey"])? $_POST["apikey"]: null;
  $user= new User(array("email" => $_SESSION["email"], "apikey" => $apikey));

  $result= UserModel::load($user, " LIMIT 1");

  if($result[0]["apikey"] == $apikey && !isset($_SESSION["apikey"]) ){
    $_SESSION["apikey"]= $apikey;
  }

?>