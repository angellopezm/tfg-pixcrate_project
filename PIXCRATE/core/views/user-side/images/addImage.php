<head>
    <script src="<?=ABS_PATH . CFGList::path_sheet['script_path']?>main/addImage.js"></script>
</head>

<?php

require_once "core/utils.php";

require_once "core/models/usermodel.php";
require_once "core/hyppos/user.php";

require_once "core/hyppos/repository.php";
require_once "core/models/repositorymodel.php";

require_once "core/hyppos/image.php";
require_once "core/models/imagemodel.php";

Utils::startSession()->checkSession()->restrictMethods();

$user = new User(array("email" => $_SESSION["email"]));
$loadedUser = UserModel::load($user);

$repoId = isset($_GET["crate"]) ? $_GET["crate"] : null;
?>

<div class="row no-gutters pixcrate-std-red">
    <div class="col-12 col-sm-2"></div>
    <div class="col-12 col-sm-8">
        <nav class="pixcrate-std-red navbar">
            <span class="navbar-brand text-white text-lg">Pixcrate</span>
        </nav>
    </div>
    <div class="col-12 col-sm-2"></div>
</div>
<div class="row no-gutters m-5">
    <div class="col-12 col-sm-12"></div>
</div>
<div class="row no-gutters">
    <div class="col-12 col-sm-2 col-lg-4"></div>
    <div class="col-12 col-sm-8 col-lg-4">
        <div class="card mt-3 pixcrate-text-dark">
            <form method="post" enctype="multipart/form-data">
                <div class="card-body">
                    <div class="input-group">
                        <div class="custom-file">
                            <label class="custom-file-label text-truncate text-muted" for="file" id="fileName">Explore <span class="text-muted font-size-8">*[Supported .png, .jpg, .gif]</span></label>
                            <input name="file" type="file" id="fileExplorer" class="custom-file-input">
                        </div>
                    </div>
                </div>
                <div class="card-footer border-bottom rounded-0">
                    <div class="form-group">
                        <label for="name">Image title</label>
                        <input placeholder="Crate name" name="name" type="text" class="validate form-control input-has-border" required>
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" cols="30" rows="5" class="validate form-control input-has-border" placeholder="Your aims, target, type of content..."></textarea>
                    </div>
                    <div class="form-group">
                        <label for="visibility">Visibility</label>
                        <select name="visibility" class="validate form-control input-has-border">
                            <option value="PRIVATE">PRIVATE</option>
                            <option value="HIDDEN">HIDDEN</option>
                            <option value="PUBLIC">PUBLIC</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="date">Publish on:</label>
                        <div class="d-flex">
                            <input type="date" name="date" class="validate form-control input-has-border w-50 h-25 mr-2 pixcrate-date">
                            <input type="time" name="time" class="validate form-control input-has-border w-50 h-25">
                        </div>
                    </div>
                    <div class="d-flex mt-3">
                        <input class="btn std-button px-4 py-2 pixcrate-std-red value-text-white" type="submit" name="submit" value="Confirm">
                    </div>
                </div>
            </form>
            <div class="card-body py-2">
                <span><a href=<?=ABS_PATH . "crate/?id=" . $repoId?>>Back «</a></span>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-2 col-lg-4"></div>
</div>

<?php
$repoId = isset($_POST["crate"]) ? $_POST["crate"] : null;

$name = isset($_POST["name"]) ? $_POST["name"] : null;
$description = isset($_POST["description"]) ? $_POST["description"] : null;
$visibility = isset($_POST["visibility"]) ? $_POST["visibility"] : null;

$file = isset($_FILES["file"]) ? $_FILES["file"] : null;

$url = ROOT_DIR . $loadedUser["username"] . "/" . $repoId . "/"; // DESTINATION

$fileParts = pathinfo($file["name"]);

// CREATE THE USER IMAGE
$imgId = Utils::generateId(rand(1, 45));
$image = new Image(["imageId" => $imgId]);
$result = ImageModel::loadAll($image);

if (isset($name) && isset($visibility) && isset($description) && isset($file)) {
    if (sizeof($result) > 0) {
        while (sizeof($result) > 0) {
            $imgId = Utils::generateId(rand(1, 45));
            $image = new Image(["imageId" => $imgId]);
            $result = ImageModel::loadAll($image);
        }
    } else {

        if (isset($fileParts)) {
            $repoId = isset($_GET["crate"]) ? $_GET["crate"] : null;
            $url = ROOT_DIR . $loadedUser["username"] . "/" . $repoId . "/" . $imgId . "." . $fileParts["extension"]; // DESTINATION
            if($fileParts["extension"] == "png" || $fileParts["extension"] == "jpg" || $fileParts["extension"] == "gif") {
                if (!file_exists($url) && !is_dir($url)) {
                    $image = new Image([
                        "imageId" => $imgId,
                        "repository_repositoryId" => $repoId,
                        "title" => $name,
                        "description" => $description,
                        "visibility" => $visibility,
                        "url" => $url,
                        "date" => date("Y-m-d"),
                        "extension" => "." . $fileParts["extension"]
                    ]);
                    ImageModel::save($image);

                    $image = new Image([
                        "imageId" => $imgId,
                    ]);
                    $result = ImageModel::loadAll($image);

                        if(sizeof($result) > 0) {
                            move_uploaded_file($file["tmp_name"], ROOT_DIR_RELATIVE . $loadedUser["username"] . "/" . $repoId . "/" . $imgId . "." . $fileParts["extension"]);
                            die("Image uploaded successfully. You can upload more images.");
                        }
                    #header("Location:" . ABS_PATH . "crate?id=" . $repoId);
                }
            }  else {
                die("The image could not be uploaded. Try again.");
            }
        }

    }

}