<?php
    require_once "core/models/usermodel.php";
    require_once "core/hyppos/user.php";
    
    require_once "core/hyppos/image.php";
    require_once "core/models/imagemodel.php";

    require_once "core/utils.php";

    Utils::startSession()->restrictMethods();

    $user = new User(array("email" => $_SESSION["email"]));
    $loadedUser = UserModel::load($user);

    $imageId = isset($_GET["id"]) ? $_GET["id"] : null;
    $image = new Image(["imageId" => $imageId]);
    $result = ImageModel::load($image);
?>

<div class="row no-gutters">
    <div class="col-12 col-sm-2 pixcrate-std-red"></div>
    <div class="col-12 col-sm-8">
        <nav class="pixcrate-std-red navbar">
            <span class="navbar-brand text-white text-lg">Pixcrate</span>
        </nav>
    </div>
    <div class="col-12 col-sm-2 pixcrate-std-red"></div>
</div>
<div class="row no-gutters m-5">
    <div class="col-12 col-sm-12"></div>
</div>
<div class="row no-gutters">
    <div class="col-12 col-sm-2 col-lg-4"></div>
    <div class="col-12 col-sm-8 col-lg-4">
        <div class="card mt-5 p-5 pixcrate-text-dark">
            <form method="post">
                <div class="form-group">
                    <label for="title">Image title</label>
                    <input placeholder="Image title" name="title" type="text" class="validate form-control input-has-border" value="<?=$result["title"]?>">
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea name="description" cols="30" rows="5" class="validate form-control input-has-border" placeholder="Your aims, target, type of content..."><?=$result["description"]?></textarea>
                </div>
                <div class="form-group">
                    <label for="visibility">Visibility</label>
                    <select name="visibility" class="validate form-control input-has-border">
                        <option value="PRIVATE" <?php if($result["visibility"]=="PRIVATE") echo "selected"?>>PRIVATE</option>
                        <option value="HIDDEN" <?php if($result["visibility"]=="HIDDEN") echo "selected"?>>HIDDEN</option>
                        <option value="PUBLIC" <?php if($result["visibility"]=="PUBLIC") echo "selected"?>>PUBLIC</option>
                    </select>
                </div>
                <div class="d-flex mt-3">
                    <input class="btn std-button px-4 py-2 pixcrate-std-red value-text-white" name="submit" type="submit" value="Confirm">
                </div>
            </form>
            <div class="mt-4">
                <span><a href=<?=ABS_PATH . "crate?id=" . $result["repository_repositoryId"]?>>Back «</a></span>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-2 col-lg-4"></div>
</div>

<?php

    $title = isset($_POST["title"]) ? $_POST["title"] : null;
    $description = isset($_POST["description"]) ? $_POST["description"] : null;
    $visibility = isset($_POST["visibility"]) ? $_POST["visibility"] : null;

    if(isset($title) && isset($description) && isset($visibility) && isset($imageId)) {
        $image = new Image([
            "imageId" => $imageId,
            "title" => $title, 
            "description" => $description, 
            "visible" => $visibility
        ]);
        ImageModel::update($image, "WHERE imageId=?");
        header("Location: " . ABS_PATH . "crate?id=" . $result["repository_repositoryId"]);
    } else if(isset($_POST["submit"]) && $_POST["submit"] == "Confirm") {
        die("Error: Could not change properties for this image. Try again or contact support.");
    }
?> 