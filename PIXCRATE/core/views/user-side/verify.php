﻿<?php
require_once "core/hyppos/user.php";
require_once "core/models/usermodel.php";

$email= isset($_GET["email"])? $_GET["email"]: null;
$hash= isset($_GET["hash"])? $_GET["hash"]: null;

if( (isset($email) && $email != "") && (isset($hash) && $hash != "") ) {
    $user = new User(["email" => $email]);
    $result = UserModel::load($user);
    if(sizeof($result) > 0 && (isset($result["hash"]) && $result["hash"] != "") ) {
        if($hash == $result["hash"]) {
            $user = new User([
                "email" => $email,
                "verified" => true
            ]);
            UserModel::update($user, " WHERE email=?");
            $result = UserModel::load($user);
            if(sizeof($result) > 0) {
                $user = new User([
                    "email" => $email,
                    "hash" => " "
                ]);
                UserModel::update($user, " WHERE email=?");
                echo "<h1 class='text-center mx-auto mt-4'>User verified correctly.</h1>";
            }
        } else {
            echo "<h1 class='text-center mx-auto mt-4'>Error: User could not be verified.</h1>";
        }
    }
} else {
    header("Location: " . ABS_PATH . "home");
}
?>