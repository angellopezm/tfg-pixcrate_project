<?php
require_once "core/utils.php";

require_once "core/models/usermodel.php";
require_once "core/hyppos/user.php";

Utils::startSession()->restrictMethods();
?>

<div class="row no-gutters">
    <div class="col-12 col-sm-2 pixcrate-std-red"></div>
    <div class="col-12 col-sm-8">
        <nav class="pixcrate-std-red navbar">
            <span class="navbar-brand text-white text-lg">Pixcrate</span>
        </nav>
    </div>
    <div class="col-12 col-sm-2 pixcrate-std-red"></div>
</div>
<div class="row no-gutters m-5">
    <div class="col-12 col-sm-12"></div>
</div>
<div class="row no-gutters">
    <div class="col-12 col-sm-2 col-lg-4"></div>
    <div class="col-12 col-sm-8 col-lg-4">
        <div class="card mt-5 p-5 pixcrate-text-dark">
            <form action="" method="post">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input placeholder="Your email" name="email" id="email" type="email" class="validate form-control input-has-border">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input placeholder="Your password" name="password" id="password" type="password" class="validate form-control input-has-border">   
                </div>
                <div class="d-flex mt-3">
                    <input class="btn std-button px-4 py-2 pixcrate-std-red value-text-white" type="submit" name="submit" value="Log in">
                </div>
            </form>
            <div class="mt-4">
                <span class="d-block">So, you don't have an account yet? <a href=<?=ABS_PATH . "register"?>>Register »</a></span>
                <span class="d-block">Have you forgot your password? <a href=<?=ABS_PATH . "password"?>>I forgot my password »</a></span>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-2 col-lg-4"></div>
</div>
<?php
$email = isset($_POST["email"]) ? $_POST["email"] : null;
$password = isset($_POST["password"]) ? $_POST["password"] : null;
$user = new User(array("email" => $email));
$result = UserModel::load($user, "LIMIT 1");
$passMatches = password_verify($password, $result["password"]);
if ($passMatches && $result["verified"]) {
    Utils::startSession();
    $_SESSION["id"] = session_id();
    $_SESSION["start_time"] = time();
    $_SESSION["email"] = $result["email"];
    $_SESSION["username"] = $result["username"];
    $_SESSION["serveradmin"] = $result["serveradmin"];
    if ($_SESSION["serveradmin"]) {
        header("Location:" . ABS_PATH . "management");
        $_SESSION["apikey"] = $result["apikey"];
    } else {
        header("Location:" . ABS_PATH . "home");
    }
}  else if(isset($_POST["submit"]) && $_POST["submit"] == "Log in") {
    die("Something went wrong. Check your email and your password.\n Also check if your account has been confirmed in order to log in.");
}
?>