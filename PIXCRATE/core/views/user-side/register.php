<head>
    <script src="<?=ABS_PATH . CFGList::path_sheet['script_path']?>jquery-ui-1.12.1.custom/jquery-ui.js"></script>
    <script src="<?=ABS_PATH . CFGList::path_sheet['script_path']?>jquery.validate.js"></script>
    <script src="<?=ABS_PATH . CFGList::path_sheet['script_path']?>additional-methods.js"></script>
    <script src="<?=ABS_PATH . CFGList::path_sheet['script_path']?>main/register.js"></script>
</head>

<?php
require_once "core/utils.php";

require_once "core/hyppos/user.php";
require_once "core/models/usermodel.php";

require_once "core/hyppos/repository.php";
require_once "core/models/repositorymodel.php";

require_once "core/hyppos/repositoryuser.php";
require_once "core/models/repositoryusermodel.php";

Utils::startSession()->restrictMethods();
?>
<div class="row no-gutters">
    <div class="col-12 col-sm-2 pixcrate-std-red"></div>
    <div class="col-12 col-sm-8">
        <nav class="pixcrate-std-red navbar">
            <span class="navbar-brand text-white text-lg">Pixcrate</span>
        </nav>
    </div>
    <div class="col-12 col-sm-2 pixcrate-std-red"></div>
</div>
<div class="row no-gutters m-5">
    <div class="col-12 col-sm-12"></div>
</div>
<div class="row no-gutters">
    <div class="col-12 col-sm-2 col-lg-4"></div>
    <div class="col-12 col-sm-8 col-lg-4">
        <div class="card mt-5 p-5 pixcrate-text-dark text-truncate">
            <form method="post" id="regForm">
                <div class="form-group d-flex">
                    <div class="d-block">
                        <label for="email" class="mt-2">Email</label>
                        <input placeholder="Your email" name="email" id="email" type="email" class="validate form-control input-has-border">
                    </div>

                    <div class="d-block mx-2">
                        <label for="username" class="mt-2">Username</label>
                        <input placeholder="Your username" name="username" id="username" type="text" class="validate form-control input-has-border">   
                    </div>   
                </div>
                <div class="form-group d-flex">
                    <div class="d-block">
                        <label for="password" class="mt-2">Password</label>
                        <input placeholder="Your password" name="password" id="password" type="password" class="validate form-control input-has-border">
                    </div>
                    <div class="d-block mx-2">
                        <label for="passwordConfirm" class="mt-2">Confirm password</label>
                        <input placeholder="Confirm your password" name="passwordConfirm" id="passwordConfirm" type="password" class="validate form-control input-has-border">
                    </div>
                </div>
                <div class="d-flex mt-4">
                    <input class="btn std-button px-4 py-2 pixcrate-std-red value-text-white submit-button" type="submit" name="submit" value="Register">
                </div>
            </form>
            <div class="mt-4">
                <span class="">If you already have an account <a href=<?=ABS_PATH . "login"?>>Log in »</a></span>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-2 col-lg-4"></div>
</div>

<?php
$email = isset($_POST["email"]) ? $_POST["email"] : null;
$username = isset($_POST["username"]) ? $_POST["username"] : null;
$password = isset($_POST["password"]) ? password_hash($_POST["password"], PASSWORD_DEFAULT) : null;
$exists = isset($_POST["exists"]) ? $_POST["exists"] : null;

if (!file_exists(ROOT_DIR_RELATIVE . $username) && !is_dir(ROOT_DIR_RELATIVE . $username) && isset($username)) {
    $hash = md5(rand(0,1000));
    $user = new User([
        "email" => $email,
        "username" => $username,
        "password" => $password,
        "profileImage" => ABS_PATH . "library/img/images/desert.jpg",
        "verified" => "false",
        "hash" => $hash,
        "apikey" => " "
    ]);
    $result = UserModel::save($user, "");

    mkdir(ROOT_DIR_RELATIVE . $username, 0777, true);
    // CREATE THE USER's FIRST REPOSITORY
    $repoId = Utils::generateId(rand(1, 45));
    $repository = new Repository(["repositoryId" => $repoId]);
    $result = RepositoryModel::loadAll($repository);
    if(sizeof($result) > 0) {
        while(sizeof($result) > 0) {
            $repoId = Utils::generateId(rand(1, 45));
            $repository = new Repository(["repositoryId" => $repoId]);
            $result = RepositoryModel::loadAll($repository);
        }
    } else {
        if (!file_exists(ROOT_DIR_RELATIVE . $username . "/" . $repoId) && !is_dir(ROOT_DIR_RELATIVE . $username . "/" . $repoId) && isset($repoId)) {
            $repository = new Repository([
                "repositoryId" => $repoId,
                "name" => $username . "'s first crate", 
                "visible" => "PRIVATE", 
                "description" => `$username's first crate. No one else but you can see this crate.`,
            ]);
            RepositoryModel::save($repository);
            $link = new Repository_Has_User([
                "repository_repositoryId" => $repoId,
                "user_email" => $email,
                "permission" => "OWNER"
            ]);
            RepositoryUserModel::save($link);
            mkdir(ROOT_DIR_RELATIVE . $username . "/" . $repoId, 0777, true);
            Utils::mail($email, "Email verification", 
                "We just sent you the link to confirm your account. Please don't reply to this, because it will not be replied anyway.\n\n" . 
                ABS_PATH . "verify?email=" . $email . "&hash=" . $hash
            );
            die("User registered. Check your email to verify your account.");
        }
    }
} else if(isset($_POST["submit"]) && $_POST["submit"] == "Register") {
    die("User already exists. If not contact with Pixcrate support.");
}
?>