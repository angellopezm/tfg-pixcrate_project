<head>
    <script src="<?=ABS_PATH . CFGList::path_sheet['script_path']?>main/addImage.js"></script>
</head>

<?php

require_once "core/utils.php";

require_once "core/models/usermodel.php";
require_once "core/hyppos/user.php";

require_once "core/hyppos/repository.php";
require_once "core/models/repositorymodel.php";

require_once "core/hyppos/image.php";
require_once "core/models/imagemodel.php";

Utils::startSession()->checkSession();

$user = new User(array("email" => $_SESSION["email"]));
$loadedUser = UserModel::load($user);

$repoId = isset($_GET["crate"]) ? $_GET["crate"] : null;
?>

<div class="row no-gutters pixcrate-std-red">
    <div class="col-12 col-sm-2"></div>
    <div class="col-12 col-sm-8">
        <nav class="pixcrate-std-red navbar">
            <span class="navbar-brand text-white text-lg">Pixcrate</span>
        </nav>
    </div>
    <div class="col-12 col-sm-2"></div>
</div>
<div class="row no-gutters m-5">
    <div class="col-12 col-sm-12"></div>
</div>
<div class="row no-gutters">
    <div class="col-12 col-sm-2 col-lg-4"></div>
    <div class="col-12 col-sm-8 col-lg-4">
        <div class="card mt-3 pixcrate-text-dark">
            <form method="post" enctype="multipart/form-data">
                <div class="card-body">
                    <div class="overflow-hide mx-auto mt-3 mb-5 rounded-circle border" style="width: 200px; height: 200px;">
                        <img src=<?=$loadedUser["profileImage"]?> class="pixcrate-circle d-inline-block m-auto" width=200 height=200>
                    </div>
                    <div class="input-group">
                        <div class="custom-file">
                            <label class="custom-file-label text-truncate text-muted" for="file" id="fileName">Explore <span class="text-muted font-size-8">*[Supported .png, .jpg, .gif]</span></label>
                            <input name="file" type="file" id="fileExplorer" class="custom-file-input">
                        </div>
                    </div>
                    <div class="d-flex mt-3">
                        <input class="btn std-button px-4 py-2 pixcrate-std-red value-text-white" type="submit" name="submit" value="Confirm">
                    </div>
                </div>
            </form>
            <div class="card-body py-2">
                <span><a href="javascript:history.back()">Back «</a></span>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-2 col-lg-4"></div>
</div>

<?php

$file = isset($_FILES["file"]) ? $_FILES["file"] : null;

$url = ROOT_DIR . $loadedUser["username"] . "/profile_images/"; // DESTINATION


    
$fileParts = pathinfo($file["name"]);

$id = 0;

if (isset($file)) {
    if (isset($fileParts)) {
        if(!file_exists(ROOT_DIR_RELATIVE . $loadedUser["username"] . "/profile_images/") && !is_dir(ROOT_DIR_RELATIVE . $loadedUser["username"] . "/profile_images/")) {
            mkdir(ROOT_DIR_RELATIVE . $loadedUser["username"] . "/profile_images/", 0777, true);
        } else {
            $repoId = isset($_GET["crate"]) ? $_GET["crate"] : null;
            $url .= "profileImage_". date("Y-m-d") . "_" . $id . "." . $fileParts["extension"]; // DESTINATION
            if($fileParts["extension"] == "png" || $fileParts["extension"] == "jpg" || $fileParts["extension"] == "gif") {
                while (file_exists($url)) {
                    $i++;
                }
                if (!file_exists($url) && !is_dir($url)) {
                    $user = new User([
                        "email" => $_SESSION["email"],
                        "profileImage" => $url
                    ]);
                    UserModel::update($user, "WHERE email=?");

                    move_uploaded_file($file["tmp_name"], ROOT_DIR_RELATIVE . $loadedUser["username"] . "/profile_images/" . "profileImage_". date("Y-m-d") . "_" . $id . "." . $fileParts["extension"]);
                    die("Image uploaded successfully. You can upload more images.");
                }
            }  else {
                die("The image could not be uploaded. Try again.");
            }
        }
    }
}