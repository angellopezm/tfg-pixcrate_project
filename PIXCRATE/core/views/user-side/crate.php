<?php

    require_once "core/utils.php";

    require_once "core/hyppos/repository.php";
    require_once "core/models/repositorymodel.php";

    require_once "core/hyppos/repositoryuser.php";
    require_once "core/models/repositoryusermodel.php";

    require_once "core/hyppos/image.php";
    require_once "core/models/imagemodel.php";

    Utils::startSession()->checkSession();

    $repositoryId= isset($_GET["id"])? $_GET["id"]: null;

    if(isset($repositoryId)) {
        $crate = new Repository(["repositoryId" => $repositoryId]);
        $thisCrate = RepositoryModel::load($crate);

        $link = new Repository_Has_User(array("user_email" => $_SESSION["email"], "repository_repositoryId" => $repositoryId));
        $linkedCrate = RepositoryUserModel::load($link);

        if($linkedCrate["permission"] == "OWNER") {
            $visibility = "";
        } else {
            $visibility = "PUBLIC";
        }

        $image = new Image([
            "repository_repositoryId" => $repositoryId,
            "visibility" => $visibility
        ]);
        $allImagesFromRep = ImageModel::loadAll($image);
?>

<div class="row no-gutters pixcrate-std-red">
    <div class="col-12 col-sm-2">
        <nav class="navbar w-100">
            <span class="navbar-brand text-white text-lg m-auto">Pixcrate</span>
        </nav>
    </div>
    <div class="col-12 col-sm-8">
        <nav class="navbar">
            <form class="form-inline d-flex flex-nowrap h-75 ml-auto mt-1" method="get" action=<?=ABS_PATH . "explore"?>>
                <input class="form-control rounded-0 pixcrate-rounded-left border-0 box-shadow-none text-truncate" type="search" placeholder="Search" 
                aria-label="Search" name="search" pattern="[A-Za-z0-9]{1,}" title="One character" required>
                <button class="my-2 my-sm-0 rounded-0 border-0 pixcrate-search-btn bg-white pixcrate-rounded-right" type="submit">
                    <i class="material-icons d-block mt-1 md-24">search</i>
                </button>
            </form>
        </nav>
    </div>
    <div class="col-12 col-sm-2"></div>
</div>
<div class="row no-gutters pixcrate-gray-light-20">
    <div class="col-12 col-sm-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb background-transparent my-auto ml-5">
                <li class="breadcrumb-item"><a href=<?=ABS_PATH . "home"?> class="underline-none pixcrate-text-dark">Home</a></li>
                <li class="breadcrumb-item"><a href=<?=ABS_PATH . "profile"?> class="underline-none pixcrate-text-dark">Profile</a></li>
                <li class="breadcrumb-item"><a href=<?=ABS_PATH . "profile/crates"?> class="underline-none pixcrate-text-dark">Crates</a></li>
                <li class="breadcrumb-item active " aria-current="page"><?=$thisCrate["name"]?></li>
            </ol>
        </nav>
    </div>
</div>
<div class="row no-gutters pixcrate-gray-light-10">
    <div class="col-12 col-sm-12">
        <a href=<?=ABS_PATH . "crate/upload?crate=" . $thisCrate["repositoryId"]?> class="btn std-button pixcrate-std-red my-2 ml-4">
            <div class="py-1">
                <i class="material-icons text-white d-inline-block align-middle">file_upload</i>
                <span class="text-white d-inline-block align-middle ml-1">Upload an image</span>
            </div>
        </a>
    </div>
</div>
<div class="row no-gutters m-auto" style="max-width: 1350px;">
    <div class="col-12 col-sm-12 d-flex flex-wrap">
        <?php 
            if(sizeof($allImagesFromRep) > 0) {
                $i= 1;
                foreach($allImagesFromRep as $row) {
        ?>
            <div class="card mx-auto mt-4 " style="width: 640px; height: 360px;">
                <form action=<?=ABS_PATH . 'crate/image'?> method="get" class="text-center underline-none overflow-hide pixcrate-border-3 rounded-top">
                    <input type="hidden" name="id" value=<?=$row["imageId"]?>>
                    <button type="submit" class="btn p-0 pixcrate-image-blur d-block w-100">
                        <img src="<?=$row["url"]?>" class="card-img-top img-fluid border-0 ">
                    </button>
                </form>
                <div class="card-footer pixcrate-gray-light-8 border-0 pixcrate-border-3 d-flex">
                    
                    <div class="d-flex mr-2">
                        <div class="m-1 p-1">
                            <span class="w-100 h-100 text-truncate center-align pixcrate-text-dark">
                                <?=$row["title"]?>
                            </span>
                        </div>
                        <div class="m-1 p-1 text-white">      
                            <span class="text-center text-truncate d-inline center-align align-middle badge badge-secondary pixcrate-std-red">
                                <?=$row["visibility"]?>
                            </span>
                        </div>
                        <button class="btn pixcrate-pane-button background-transparent pixcrate-text-dark mx-1 p-1">
                            <span class="text-center text-truncate d-inline align-middle">
                                <?=$row["likes"]?>
                            </span>
                            <i class="material-icons align-middle">thumb_up</i>
                        </button>
                        <button class="btn pixcrate-pane-button background-transparent pixcrate-text-dark mx-1 p-1">
                            <span class="text-center text-truncate d-inline center-align align-middle">
                                <?=$row["reposts"]?>
                            </span>
                            <i class="material-icons d-inline align-middle">repeat</i>
                        </button>
                    </div>
                    <?php
                    if($linkedCrate["permission"] == "OWNER") {
                    ?>
                        <div>
                            <a href=<?=ABS_PATH . "crate/delete_image?id=" . $row["imageId"]?> class="btn background-transparent pixcrate-pane-button">
                                <i class="material-icons d-inline align-middle">delete</i>
                            </a>
                        </div>

                        <div>
                            <a href=<?=ABS_PATH . "crate/edit_image?id=" . $row["imageId"]?> class="btn background-transparent pixcrate-pane-button">
                                <i class="material-icons d-inline align-middle">edit</i>
                            </a>
                        </div>     
                    <?php
                    }
                    ?>
                </div>
            </div>
        <?php
                }
            } else {
                echo 
                    "<div class='d-flex mx-auto mt-5 w-50 flex-wrap center-align'>" .
                        "<h1 class='mx-auto'>There's no images here!</h1>" .
                        "<p class='mx-auto my-2 text-muted'>Try to upload some images by clicking the \"Upload an image\" buttom above.</p>" .
                    "</div>"
                ;
            }
        ?>
    </div>
</div>
            
<div class="row no-gutters py-3">
    <div class="col-12 col-sm-12"></div>
</div>

<?php
            
    } else {
        header("Location:" . ABS_PATH . "missing");
    }

?>