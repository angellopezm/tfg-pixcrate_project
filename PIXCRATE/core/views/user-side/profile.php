<head>
    <script src="<?=ABS_PATH . CFGList::path_sheet['script_path']?>main/profile.js"></script>
</head>

<?php 
require_once "core/utils.php";

require_once "core/models/usermodel.php";
require_once "core/hyppos/user.php";

require_once "core/hyppos/repository.php";
require_once "core/models/repositorymodel.php";

require_once "core/hyppos/repositoryuser.php";
require_once "core/models/repositoryusermodel.php";

require_once "core/hyppos/image.php";
require_once "core/models/imagemodel.php";


Utils::startSession()->checkSession();
$user = new User(array("email" => $_SESSION["email"]));
$loadedUser = UserModel::load($user);
$profileImage = $loadedUser["profileImage"];

$link = new Repository_Has_User(array("user_email" => $_SESSION["email"]));
$linkedCrates = RepositoryUserModel::loadAll($link);

$totalImages = 0;
$totalOwnedImages = 0;

foreach ($linkedCrates as $row) {

    if($row["permission"] == "OWNER") {
        $visibility = "";
    } else {
        $visibility = "PUBLIC";
    }
    
    $image = new Image([
        "repository_repositoryId" => $row["repository_repositoryId"],
        "visibility" => $visibility
    ]);

    if($row["permission"] == "OWNER") {
        $allImagesFromRep = ImageModel::loadAll($image);
        $totalOwnedImages+= sizeof($allImagesFromRep);
    } 

    $allImagesFromRep = ImageModel::loadAll($image);
    $totalImages+= sizeof($allImagesFromRep);
}

?>

<div class="row no-gutters pixcrate-std-red">
    <div class="col-12 col-sm-2">
        <nav class="navbar w-100">
            <span class="navbar-brand text-white text-lg m-auto">Pixcrate</span>
        </nav>
    </div>
    <div class="col-12 col-sm-8">
        <nav class="navbar">
            <form class="form-inline d-flex flex-nowrap h-75 ml-auto mt-1" method="get" action=<?=ABS_PATH . "explore"?>>
                <input class="form-control rounded-0 pixcrate-rounded-left border-0 box-shadow-none text-truncate" type="search" placeholder="Search" 
                aria-label="Search" name="search" pattern="[A-Za-z0-9]{1,}" title="One character" required>
                <button class="my-2 my-sm-0 rounded-0 border-0 pixcrate-search-btn bg-white pixcrate-rounded-right" type="submit">
                    <i class="material-icons d-block mt-1 md-24">search</i>
                </button>
            </form>
        </nav>
    </div>
    <div class="col-12 col-sm-2"></div>
</div>
<div class="row no-gutters pixcrate-gray-light-20">
    <div class="col-12 col-sm-4">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb background-transparent my-auto ml-5">
                <li class="breadcrumb-item"><a href=<?=ABS_PATH . "home"?> class="underline-none pixcrate-text-dark">Home</a></li>
                <li class="breadcrumb-item active " aria-current="page">Profile</li>
            </ol>
        </nav>
    </div>
    <div class="col-12 col-sm-4">
    </div>
    <div class="col-12 col-sm-4"></div>
</div>
<div class="row no-gutters pixcrate-gray-light-6">
    <div class="col-12 col-sm-4 d-flex flex-wrap pixcrate-gray-light-4">
        <div class="mt-6 mb-5 ml-6 overflow-hide">
            <?php
                if(isset($profileImage) && $profileImage != ""){
            ?>
                <img src=<?=$profileImage?> alt="profile-image" class="pixcrate-circle d-inline-block m-auto" width=200 height=200>
            <?php
                } else {
            ?>
                <img src=<?=CFGList::path_sheet["img_path"]."images/desert.jpg"?> alt="profile-image" class="pixcrate-circle d-inline-block m-auto" width=200 height=200>
            <?php
                }
            ?>
        </div>
        <div class="card background-transparent border-0 mt-6 mb-5 ml-5">
            <div class="card-body">
                <h2 class="font-weight-bold pixcrate-text-dark pixcrate-clock"></h2>
                <h4 class="card-title pixcrate-text-dark"><?=$loadedUser["username"]?></h5>
                <h5 class="card-subtitle mb-2 text-muted"><?=$loadedUser["name"]?> <?=$loadedUser["surname"]?></h6>
                <h6 class="card-subtitle mb-2 text-muted font-size-8">Birth date: <?=date("d-m-Y", strtotime($loadedUser["birth"]))?></h6>
                <div>
                    <a href=<?=ABS_PATH . "password"?> class="card-link d-block" id="changePasswordLink">Change password</a>
                    <a href=<?=ABS_PATH . "profile/profile_image"?> class="card-link d-block m-0" id="uploadProfileImageLink">Upload profile image</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-5 border-right pixcrate-border-dark">
        <div class="ml-4 mt-3 d-block">
            <a href=<?=ABS_PATH . "profile/crates"?> class="btn std-button pixcrate-std-red py-3 px-5 m-2" id="cratesLink"><span class="text-white">Crates</span></a>
            <a href=<?=ABS_PATH . "logout"?> class="btn std-button pixcrate-std-red py-3 px-5 m-2" id="logoutLink"><span class="text-white">Log out</span></a>
            <?php
                if(isset($loadedUser["apikey"]) && $loadedUser["apikey"] != " ") {
            ?>
                    <button class="btn std-button pixcrate-text-dark pixcrate-gray-light-30 border-0 rounded-0 pixcrate-rounded-left pixcrate-rounded-right py-2 px-4 m-2" 
                    type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" id="accordion">
                        <i class="material-icons d-inline-block my-2">remove_red_eye</i>
                    </button>
            <?php
                }
            ?>
            <div id="collapseOne" class="collapse input-has-border pixcrate-rounded-right pixcrate-rounded-left pixcrate-gray-light-10 text-center ml-2 mr-4 text-truncate" aria-labelledby="headingOne" data-parent="#accordion">
                <span class="text-muted d-inline-block pt-1">
                    <?=$loadedUser["apikey"]?>
                </span>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-3 pixcrate-gray-light-4">
        <div class="p-5 w-100 h-100 pixcrate-text-dark pixcrate-border-dark pixcrate-gray-light-4" id="hintHolder">
            <p id="hintParagraph"></p>
        </div>
    </div>
</div>

<div class="row no-gutters pixcrate-gray-light-4 pixcrate-text-dark d-flex">
    <div class="text-center py-5 px-5 mx-auto">
        <i class="material-icons">inbox</i>
        <h1 class="font-weight-bold m-0 d-inline">
            <?=sizeof($linkedCrates)?>
        </h1>
        <span class="d-inline h3">links</span>
        <p class="text-muted">with crates overall</p>
    </div>
    <div class="text-center py-5 px-5 mx-auto">
        <i class="material-icons">photo</i>
        <h1 class="font-weight-bold m-0 d-inline">
            <?=$totalImages?>
        </h1>
        <span class="d-inline h3">links</span>
        <p class="text-muted">with images overall</p>
    </div>
    <div class="text-center py-5 px-5 mx-auto">
        <i class="material-icons">photo</i>
        <h1 class="font-weight-bold m-0 d-inline">
            <?=$totalOwnedImages?>
        </h1>
        <span class="d-inline h3">images</span>
        <p class="text-muted">owned overall</p>
    </div>
    <div class=""></div>
</div>