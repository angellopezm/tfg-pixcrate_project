<head>
    <link rel="stylesheet" href="<?=ABS_PATH . CFGList::path_sheet['style_path']?>home.css">
    <script src="<?=ABS_PATH . CFGList::path_sheet['script_path']?>main/home.js"></script>
</head>

<?php
require_once "core/utils.php";

require_once "core/models/usermodel.php";
require_once "core/hyppos/user.php";

require_once "core/hyppos/repository.php";
require_once "core/models/repositorymodel.php";

require_once "core/hyppos/repositoryuser.php";
require_once "core/models/repositoryusermodel.php";

require_once "core/hyppos/image.php";
require_once "core/models/imagemodel.php";

Utils::startSession()->checkSession();
$user = new User(array("email" => $_SESSION["email"]));
$loadedUser = UserModel::load($user);

$profileImage = $loadedUser["profileImage"];
$visibility = "";

$obj = new Repository_Has_User([
    "user_email" => $_SESSION["email"]
]);
$resultLinks = RepositoryUserModel::loadAll($obj);

$resultCrates = [];
$resultImages = [];

foreach ($resultLinks as $row) {

    $obj = new Repository([
        "repositoryId" => $row["repository_repositoryId"]
    ]);
    $result = RepositoryModel::load($obj);

    array_push($resultCrates, $result);
}

?>
<div class="row no-gutters pixcrate-std-red">
    <div class="col-12 col-sm-2">
        <nav class="navbar w-100">
            <span class="navbar-brand text-white text-lg m-auto">Pixcrate</span>
        </nav>
    </div>
    <div class="col-12 col-sm-8">
        <nav class="navbar">
            <form class="form-inline d-flex flex-nowrap h-75 ml-auto mt-1" method="get" action=<?=ABS_PATH . "explore"?>>
                <input class="form-control rounded-0 pixcrate-rounded-left border-0 box-shadow-none text-truncate" type="search" placeholder="Search" 
                aria-label="Search" name="search" pattern="[A-Za-z0-9]{1,}" title="One character" required>
                <button class="my-2 my-sm-0 rounded-0 border-0 pixcrate-search-btn bg-white pixcrate-rounded-right" type="submit">
                    <i class="material-icons d-block mt-1 md-24">search</i>
                </button>
            </form>
        </nav>
    </div>
    <div class="col-12 col-sm-2 pixcrate-std-red"></div>
</div>
<div class="row no-gutters border-bottom pixcrate-gray-light-12 border-0">
    <div class="col-12">
        <div class="card border-0 background-transparent">
            <div class="card-body d-flex border-left py-4 px-5">
                <a href="<?=ABS_PATH . "profile"?>" class="rounded-circle overflow-hide" id="profileImageHolder">
                    <img src=<?=$profileImage?> alt="" height=110 width=110 class="m-auto d-block position-relative" id="profileImage">
                </a>
                <div class="pt-4 text-truncate d-inline-block pixcrate-text-dark">
                    <span class="align-middle underline-none d-block my-auto mx-4"><?=$_SESSION["username"]?></span>
                    <span class="align-middle d-block font-weight-bold mx-4 pixcrate-clock"></span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row no-gutters pixcrate-gray-light-4">
    <div class="col-12 col-sm-2">
        <div class="d-flex m-auto pixcrate-gray-light-4 border-0 text-truncate">
            <a href="<?=ABS_PATH . "profile"?>" class="pixcrate-pane-button text-center underline-none text-truncate py-1">
                <i class="material-icons d-block">person</i>
                <span class="d-block">Profile</span>
            </a>
            <a href="<?=ABS_PATH . "profile/crates"?>" class="pixcrate-pane-button text-center underline-none text-truncate py-1">
                <i class="material-icons d-block">inbox</i>
                <span class="d-block">Crates</span>
            </a>
            <?php
                if($_SESSION["serveradmin"] != 0) {
            ?>
                    <a href="<?=ABS_PATH . "management"?>" class="pixcrate-pane-button text-center underline-none text-truncate py-1">
                        <i class="material-icons d-block">build</i>
                        <span class="d-block">Management</span>
                    </a> 
            <?php 
                }
            ?>
        </div>
    </div>
    <div class="col-12 col-sm-8"> 
    </div>
    <div class="col-12 col-sm-2">
        <div class="d-flex pixcrate-gray-light-4 rounded-bottom border-0">
            <a href="<?=ABS_PATH . "about"?>" class="pixcrate-pane-button text-center underline-none text-truncate py-1">
                <i class="material-icons d-block">help_outline</i>
                <span class="d-block">About</span>
            </a>
            <a href="<?=ABS_PATH . "logout"?>" class="pixcrate-pane-button text-center underline-none text-truncate py-1">
                <i class="material-icons d-block">exit_to_app</i>
                <span class="d-block">Exit</span>
            </a>
        </div>
    </div>
</div>

<div class="row no-gutters">
    <div class="col-12 col-sm-2"></div>
    <div class="col-12 col-sm-8">
        <?php
        include_once "core/views/public-side/results/crateresults.php";
        ?>
    </div>
    <div class="col-12 col-sm-2"></div>
</div>