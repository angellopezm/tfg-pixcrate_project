<?php
    require_once "core/models/usermodel.php";
    require_once "core/hyppos/user.php";

    require_once "core/hyppos/repository.php";
    require_once "core/models/repositorymodel.php";
    
    require_once "core/hyppos/repositoryuser.php";
    require_once "core/models/repositoryusermodel.php";

    require_once "core/utils.php";

    Utils::startSession()->restrictMethods();


    $user = new User(array("email" => $_SESSION["email"]));
    $loadedUser = UserModel::load($user);

    $repoId = isset($_GET["crate"]) ? $_GET["crate"] : null;
    $repository = new Repository(["repositoryId" => $repoId]);
    $result = RepositoryModel::load($repository);
?>

<div class="row no-gutters">
    <div class="col-12 col-sm-2 pixcrate-std-red"></div>
    <div class="col-12 col-sm-8">
        <nav class="pixcrate-std-red navbar">
            <span class="navbar-brand text-white text-lg">Pixcrate</span>
        </nav>
    </div>
    <div class="col-12 col-sm-2 pixcrate-std-red"></div>
</div>
<div class="row no-gutters m-5">
    <div class="col-12 col-sm-12"></div>
</div>
<div class="row no-gutters">
    <div class="col-12 col-sm-2 col-lg-4"></div>
    <div class="col-12 col-sm-8 col-lg-4">
        <div class="card mt-5 p-5 pixcrate-text-dark">
            <form method="post">
                <div class="form-group">
                    <label for="name">Crate name</label>
                    <input placeholder="Crate name" name="name" type="text" class="validate form-control input-has-border" value="<?=$result["name"]?>">
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea name="description" cols="30" rows="5" class="validate form-control input-has-border" placeholder="Your aims, target, type of content..."><?=$result["description"]?></textarea>
                </div>
                <div class="form-group">
                    <label for="visibility">Visibility</label>
                    <select name="visibility" class="validate form-control input-has-border">
                        <option value="PRIVATE" <?php if($result["visible"]=="PRIVATE") echo "selected"?>>PRIVATE</option>
                        <option value="HIDDEN" <?php if($result["visible"]=="HIDDEN") echo "selected"?>>HIDDEN</option>
                        <option value="PUBLIC" <?php if($result["visible"]=="PUBLIC") echo "selected"?>>PUBLIC</option>
                    </select>
                </div>
                <div class="d-flex mt-3">
                    <input class="btn std-button px-4 py-2 pixcrate-std-red value-text-white" type="submit" value="Confirm">
                </div>
            </form>
            <div class="mt-4">
                <span><a href=<?=ABS_PATH . "profile/crates"?>>Back «</a></span>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-2 col-lg-4"></div>
</div>

<?php

    $name = isset($_POST["name"]) ? $_POST["name"] : null;
    $description = isset($_POST["description"]) ? $_POST["description"] : null;
    $visibility = isset($_POST["visibility"]) ? $_POST["visibility"] : null;

    if(isset($name) && isset($description) && isset($visibility) && isset($repoId)) {
        $repository = new Repository([
            "repositoryId" => $repoId,
            "name" => $name, 
            "description" => $description, 
            "visible" => $visibility
        ]);
        RepositoryModel::update($repository, "WHERE repositoryId=?");
        header("Location: " . ABS_PATH . "profile/crates");
    }
?>