<?php
require_once "core/utils.php";

require_once "core/models/usermodel.php";
require_once "core/hyppos/user.php";

require_once "core/hyppos/repository.php";
require_once "core/models/repositorymodel.php";

require_once "core/hyppos/repositoryuser.php";
require_once "core/models/repositoryusermodel.php";

Utils::startSession()->restrictMethods();
$user = new User(array("email" => $_SESSION["email"]));
$loadedUser = UserModel::load($user);
?>

<div class="row no-gutters">
    <div class="col-12 col-sm-2 pixcrate-std-red"></div>
    <div class="col-12 col-sm-8">
        <nav class="pixcrate-std-red navbar">
            <span class="navbar-brand text-white text-lg">Pixcrate</span>
        </nav>
    </div>
    <div class="col-12 col-sm-2 pixcrate-std-red"></div>
</div>
<div class="row no-gutters m-5">
    <div class="col-12 col-sm-12"></div>
</div>
<div class="row no-gutters">
    <div class="col-12 col-sm-2 col-lg-4"></div>
    <div class="col-12 col-sm-8 col-lg-4">
        <div class="card mt-5 p-5 pixcrate-text-dark">
            <form method="post">
                <div class="form-group">
                    <label for="name">Crate name</label>
                    <input placeholder="Crate name" name="name" type="text" class="validate form-control input-has-border">
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea name="description" cols="30" rows="5" class="validate form-control input-has-border" placeholder="Your aims, target, type of content..."></textarea>
                </div>
                <div class="form-group">
                    <label for="visibility">Visibility</label>
                    <select name="visibility" class="validate form-control input-has-border">
                        <option value="PRIVATE">PRIVATE</option>
                        <option value="HIDDEN">HIDDEN</option>
                        <option value="PUBLIC">PUBLIC</option>
                    </select>
                </div>
                <div class="d-flex mt-3">
                    <input class="btn std-button px-4 py-2 pixcrate-std-red value-text-white" type="submit" value="Confirm">
                </div>
            </form>
            <div class="mt-4">
                <span><a href=<?=ABS_PATH . "profile/crates"?>>Back «</a></span>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-2 col-lg-4"></div>
</div>

<?php
$name = isset($_POST["name"]) ? $_POST["name"] : null;
$description = isset($_POST["description"]) ? $_POST["description"] : null;
$visibility = isset($_POST["visibility"]) ? $_POST["visibility"] : null;
// CREATE THE USER's FIRST REPOSITORY
$repoId = Utils::generateId(rand(1, 45));
$repository = new Repository(["repositoryId" => $repoId]);
$result = RepositoryModel::loadAll($repository);
if(isset($name) && isset($visibility)) {
    if (sizeof($result) > 0) {
        while (sizeof($result) > 0) {
            $repoId = Utils::generateId(rand(1, 45));
            $repository = new Repository(["repositoryId" => $repoId]);
            $result = RepositoryModel::loadAll($repository);
        }
    } else {
        if (!file_exists(ROOT_DIR_RELATIVE . $loadedUser["username"] . "/" . $repoId) && !is_dir(ROOT_DIR_RELATIVE . $loadedUser["username"] . "/" . $repoId) && isset($repoId)) {
            $repository = new Repository([
                "repositoryId" => $repoId,
                "name" => $name,
                "description" => $description,
                "visible" => $visibility,
            ]);
            RepositoryModel::save($repository);
            $link = new Repository_Has_User([
                "repository_repositoryId" => $repoId,
                "user_email" => $loadedUser["email"],
                "permission" => "OWNER",
            ]);
            RepositoryUserModel::save($link);
            mkdir(ROOT_DIR_RELATIVE . $loadedUser["username"] . "/" . $repoId, 0777, true);
            header("Location: " . ABS_PATH . "profile/crates");
        }
    }
}
?>