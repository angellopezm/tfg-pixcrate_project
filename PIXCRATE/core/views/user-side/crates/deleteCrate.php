<?php
require_once "core/utils.php";

require_once "core/models/usermodel.php";
require_once "core/hyppos/user.php";

require_once "core/hyppos/repository.php";
require_once "core/models/repositorymodel.php";

require_once "core/hyppos/repositoryuser.php";
require_once "core/models/repositoryusermodel.php";

Utils::startSession()->restrictMethods();
$user = new User(array("email" => $_SESSION["email"]));
$loadedUser = UserModel::load($user);

?>

<div class="row no-gutters">
    <div class="col-12 col-sm-2 pixcrate-std-red"></div>
    <div class="col-12 col-sm-8">
        <nav class="pixcrate-std-red navbar">
            <span class="navbar-brand text-white text-lg">Pixcrate</span>
        </nav>
    </div>
    <div class="col-12 col-sm-2 pixcrate-std-red"></div>
</div>
<div class="row no-gutters m-5">
    <div class="col-12 col-sm-12"></div>
</div>
<div class="row no-gutters">
    <div class="col-12 col-sm-2 col-lg-4"></div>
    <div class="col-12 col-sm-8 col-lg-4">
        <div class="card mt-5 p-5 pixcrate-text-dark">
            <form method="post">
                <span>Are you sure you want to delete this repository?</span>
                <div class="d-flex mt-3">
                    <input class="btn std-button px-4 py-2 pixcrate-std-red value-text-white mr-4" type="submit" name="confirm" value="Confirm">
                    <input class="btn std-button px-4 py-2 pixcrate-std-red value-text-white" type="submit" name="cancel" value="Cancel">
                </div>
            </form>
            <div class="mt-4">
                <span><a href=<?=ABS_PATH . "profile/crates"?>>Back «</a></span>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-2 col-lg-4"></div>
</div>

<?php
$confirm = isset($_POST["confirm"]) ? $_POST["confirm"] : null;
$cancel = isset($_POST["cancel"]) ? $_POST["cancel"] : null;
$repoId = isset($_GET["crate"]) ? $_GET["crate"] : null;

if(isset($confirm)) {
    if (file_exists(ROOT_DIR_RELATIVE . $loadedUser["username"] . "/" . $repoId) && is_dir(ROOT_DIR_RELATIVE . $loadedUser["username"] . "/" . $repoId) && isset($repoId)) {
        $link = new Repository_Has_User([
            "repository_repositoryId" => $repoId
        ]);
        RepositoryUserModel::delete($link);
        $repository = new Repository([
            "repositoryId" => $repoId
        ]);
        RepositoryModel::delete($repository);
        Utils::rmdir_recursive(ROOT_DIR_RELATIVE . $loadedUser["username"] . "/" . $repoId);
    }
    header("Location: " . ABS_PATH . "profile/crates");
} else if (isset($cancel)) {
    header("Location: " . ABS_PATH . "profile/crates");
}
?>