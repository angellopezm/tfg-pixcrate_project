<?php
require_once "core/utils.php";

Utils::startSession()->restrictMethods();    

?>

<div class="row no-gutters pixcrate-std-red">
    <div class="col-12 col-sm-2 "></div>
    <div class="col-12 col-sm-8">
        <nav class="pixcrate-std-red navbar w-100">
        <span class="navbar-brand text-white text-lg">Pixcrate</span>
        <div class="right-align">
            <a href="<?=ABS_PATH . "login"?>" class="d-inline text-white underline-none mr-2">Log in</a>
            <a href="<?=ABS_PATH . "register"?>" class="d-inline text-white underline-none mr-2">Register</a>
            <a href="<?=ABS_PATH . "about"?>" class="d-inline text-white underline-none ">About</a>
        </div>
        </nav>
    </div>
    <div class="col-12 col-sm-2"></div>
</div>
<div class="row no-gutters">
    <div class="col-12 col-sm-12 text-center overflow-hide">
        <h1 class="pt-5 font-weight-bold">Welcome to Pixcrate</h1>
        <p class="m-5 text-muted">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec volutpat vel velit ut vulputate. Vestibulum sit amet sapien in est gravida porttitor vel vitae felis. Fusce sit amet porta mauris. Donec ut tellus lorem. Morbi sagittis justo a neque interdum sodales. Sed auctor lectus ac purus interdum, ac consequat neque ultricies. Aenean egestas in dui ac tincidunt. Ut bibendum tortor a magna rhoncus, a efficitur tortor mattis. Donec mi augue, tempus in maximus hendrerit, viverra nec nisi. Nulla facilisi. Etiam quis massa libero. Pellentesque at ullamcorper augue, feugiat accumsan sapien. Maecenas tristique lobortis nunc nec ultrices. Pellentesque consectetur convallis tincidunt. Maecenas imperdiet nibh eget urna viverra rhoncus. Duis euismod viverra lorem, in lacinia leo condimentum sed.
        </p>
    </div>
</div>
<div class="row no-gutters pixcrate-gray-light-8">
    <div class="col-12 col-sm-4 img-container">
        <img src="<?=ABS_PATH . CFGList::path_sheet['img_path'] . "images/desert.jpg"?>" alt="" class="img-fluid">
    </div>
    <div class="col-12 col-sm-2 img-container right-align"></div>
    <div class="col-12 col-sm-6 text-left overflow-hide">
        <h1 class="m-5">Proin pharetra</h1>
        <p class="m-5 text-muted">
            Proin pharetra commodo sem. Pellentesque porttitor porta leo, eget sodales diam ornare eget. Nunc varius arcu quis nunc vehicula posuere. Aliquam ut elit porta, ornare velit nec, lobortis elit. Integer elementum massa non nisl viverra, nec cursus lectus dignissim. Integer nibh tellus, ullamcorper et lectus sit amet, sollicitudin varius purus. Donec porttitor euismod lorem, nec bibendum eros varius sed. Morbi tincidunt, odio a elementum egestas, est turpis feugiat est, at mollis arcu turpis eu quam. Proin ac sagittis magna, vel placerat libero. Donec leo ex, malesuada ac risus eget, ullamcorper iaculis neque. Ut congue hendrerit sapien, id porta sem. Ut ornare lectus non nisi dictum, eu tempus lectus tincidunt.
        </p>
    </div>
</div>
<div class="row no-gutters">
    <div class="col-12 col-sm-12 text-center overflow-hide">
        <p class="m-5 text-muted">
            Nulla at vulputate odio. Integer pulvinar ex ac quam tempus, mollis pulvinar orci dapibus. Mauris feugiat ac augue at interdum. Mauris ac lacus eget urna tempor volutpat ut vitae ipsum. Duis rutrum ut eros vestibulum viverra. Vestibulum sit amet nisl at metus egestas suscipit. Phasellus commodo eros ac ante mattis, congue luctus orci faucibus. Phasellus vulputate sem in libero auctor feugiat. Integer aliquam euismod sem. Nam mollis vel eros ac tristique. Fusce sit amet nunc eu lectus molestie elementum nec id leo. Donec quis varius ex, eget dignissim erat. 
        </p>
    </div>
</div>
<div class="row no-gutters pixcrate-gray-light-8">
    <div class="col-12 col-sm-6 text-left overflow-hide">
        <h1 class="m-5">Integer sit</h1>
        <p class="m-5 text-muted">
            Integer sit amet leo vel neque vestibulum condimentum. Proin lacus odio, vehicula eu dapibus sed, vehicula id lacus. Nam aliquet elit sed nunc volutpat sagittis. Maecenas id faucibus arcu. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla scelerisque dui et tristique sollicitudin. Curabitur luctus orci ut ligula pellentesque, vel imperdiet ipsum ultricies. Maecenas vel nisl eu ante tincidunt aliquet hendrerit venenatis purus.
        </p>
    </div>
    <div class="col-12 col-sm-2"></div>
    <div class="col-12 col-sm-4 img-container right-align">
        <img src="<?=ABS_PATH . CFGList::path_sheet['img_path'] . "images/hydrangeas.jpg"?>" alt="" class="img-fluid">
    </div>
</div>
<div class="row no-gutters">
    <div class="col-12 col-sm-4"></div>
    <div class="col-12 col-sm-4 text-center">
        <?php
            if(!isset($_SESSION["id"])) {
        ?>
                <div class="d-flex m-5">
                <a href=<?=ABS_PATH . "login"?> class="btn text-white underline-none std-button pixcrate-std-red mx-auto w-50 py-3">
                    <span>Login now</span>
                </a>
                <span class="text-center d-inline-block my-auto mx-4 text-muted">OR</span>
                <a href=<?=ABS_PATH . "register"?> class="btn text-white underline-none std-button pixcrate-std-red mx-auto w-50 py-3">
                    <span>Register</span>
                </a>
                </div>
        <?php 
            }
        ?>
    </div>
    <div class="col-12 col-sm-4"></div>
</div>
<div class="row no-gutters pixcrate-std-red position-static p-5 border-top border-light">
    <div class="col-12 col-sm-2"></div>
    <div class="col-12 col-sm-8 p-2 d-flex">
        <a href=<?=ABS_PATH . "about"?> class="text-white underline-none mx-3">About-us</a>
    </div>
    <div class="col-12 col-sm-2"></div>
</div>