<?php 

    require_once "core/utils.php";

    require_once "core/models/usermodel.php";
    require_once "core/hyppos/user.php";

    require_once "core/hyppos/repository.php";
    require_once "core/models/repositorymodel.php";
    
    require_once "core/hyppos/repositoryuser.php";
    require_once "core/models/repositoryusermodel.php";

    require_once "core/hyppos/image.php";
    require_once "core/models/imagemodel.php";

    Utils::startSession()->checkSession();

    $user = new User(array("email" => $_SESSION["email"]));
    $loadedUser = UserModel::load($user);

    $link = new Repository_Has_User(array("user_email" => $_SESSION["email"]));
    $linkedCrates = RepositoryUserModel::loadAll($link);

    $userCrates= [];

    for ($i = 0; $i < sizeof($linkedCrates); $i++) {

        $crate = new Repository([
            "repositoryId" => $linkedCrates[$i]["repository_repositoryId"],
        ]);
        array_push($userCrates, RepositoryModel::load($crate));
    }

?>

<div class="row no-gutters pixcrate-std-red">
    <div class="col-12 col-sm-2">
        <nav class="navbar w-100">
            <span class="navbar-brand text-white text-lg m-auto">Pixcrate</span>
        </nav>
    </div>
    <div class="col-12 col-sm-8">
        <nav class="navbar">
            <form class="form-inline d-flex flex-nowrap h-75 ml-auto mt-1" method="get" action=<?=ABS_PATH . "explore"?>>
                <input class="form-control rounded-0 pixcrate-rounded-left border-0 box-shadow-none text-truncate" type="search" placeholder="Search" 
                aria-label="Search" name="search" pattern="[A-Za-z0-9]{1,}" title="One character" required>
                <button class="my-2 my-sm-0 rounded-0 border-0 pixcrate-search-btn bg-white pixcrate-rounded-right" type="submit">
                    <i class="material-icons d-block mt-1 md-24">search</i>
                </button>
            </form>
        </nav>
    </div>
    <div class="col-12 col-sm-2"></div>
</div>
<div class="row no-gutters pixcrate-gray-light-20">
    <div class="col-12 col-sm-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb background-transparent my-auto ml-5">
                <li class="breadcrumb-item"><a href=<?=ABS_PATH . "home"?> class="underline-none pixcrate-text-dark">Home</a></li>
                <li class="breadcrumb-item"><a href=<?=ABS_PATH . "profile"?> class="underline-none pixcrate-text-dark">Profile</a></li>
                <li class="breadcrumb-item active " aria-current="page">Crates</li>
            </ol>
        </nav>
    </div>
</div>
<div class="row no-gutters pixcrate-gray-light-10">
    <div class="col-12 col-sm-12">
        <a href=<?=ABS_PATH . "profile/create_repository"?> class="btn std-button pixcrate-std-red my-2 ml-4">
            <div class="py-1">
                <i class="material-icons text-white d-inline-block align-middle">create_new_folder</i>
                <span class="text-white d-inline-block align-middle ml-1">Open a new crate</span>
            </div>
        </a>
    </div>
</div>
<div class="row no-gutters m-auto">
    <div class="col-12 col-sm-9 card rounded-0 border-0">
        <?php 
            $i= 1;
            foreach($userCrates as $row) {

                $link = new Repository_Has_User(array("repository_repositoryId" => $row["repositoryId"]));
                $linkedCrates = RepositoryUserModel::load($link);

                if($linkedCrates["permission"] == "OWNER") {
                    $visibility = "";
                } else {
                    $visibility = "PUBLIC";
                }

                $image = new Image([
                    "repository_repositoryId" => $row["repositoryId"],
                    "visibility" => $visibility
                ]);
                $allImagesFromRep = ImageModel::loadAll($image, " LIMIT 4");
        ?>
        <div class="border-bottom container-fluid p-0">
                <div class="row no-gutters">
                    <a href=<?=ABS_PATH . 'crate?id=' . $row["repositoryId"]?> class="col-12 col-xl-11 col-lg-10 col-md-9 py-3 pl-4 rounded-0 pixcrate-text-dark d-flex underline-none">
                        <div class="border rounded overflow-hide pixcrate-border-dark pixcrate-gray-light-10" style="width: 320px; height: 180px;">
                            <?php
                                if(sizeof($allImagesFromRep) > 1) {
                            ?>
                            <div class="carousel slide" data-ride="carousel" data-interval="3000">
                                <div class="carousel-inner">
                                    <?php
                                        $i = 0;
                                        foreach($allImagesFromRep as $image) {
                                    ?>  
                                    <div class="carousel-item center-align<?php if($i == 0 ) echo " active";?>">
                                        <img src="<?=$allImagesFromRep[$i]["url"]?>" width=320px>
                                    </div>
                                    <?php   
                                            $i++;
                                        }
                                    ?>
                                </div>
                            </div>
                            <?php
                                } else if(sizeof($allImagesFromRep) == 1){
                            ?>
                            <img src="<?=$allImagesFromRep[0]["url"]?>" width=320px >
                            <?php
                                } else {
                            ?>
                            <div class="m-auto text-center">
                                <span class="align-middle">
                                    No images found
                                </span>
                            </div>
                            <?php
                                }
                            ?>
                        </div>
                        <div class="ml-4 mt-4 pr-3">
                            <p class="center-align pixcrate-text-dark font-weight-bold h5">
                                <?=$row["name"]?>
                                <span class="text-center text-truncate d-inline center-align align-middle badge badge-secondary pixcrate-std-red" style="font-size:.5em">
                                    <?=$row["visible"]?>
                                </span>
                            </p>
                            <p class="center-align pixcrate-text-dark pixcrate-text-wrap">
                                <?=$row["description"]?>
                            </p>
                        </div>
                    </a>
                    <div class="col-12 col-xl-1 col-lg-2 col-md-3 m-auto border-left">
                        <?php
                            if ($linkedCrates["permission"] == "OWNER") {
                        ?>
                                <a href=<?=ABS_PATH . "profile/edit_repository?crate=" . $row["repositoryId"]?> class="btn background-transparent pixcrate-pane-button d-block rounded-0">
                                    <span class="text-center text-truncate d-block align-middle pixcrate-text-dark mr-1">
                                        Edit
                                    </span>
                                    <i class="material-icons d-block align-middle pixcrate-text-dark">edit</i>
                                </a>
                        
                        <?php
                            }
                            if ($linkedCrates["permission"] == "OWNER" || $linkedCrates["permission"] == "ADMIN") {
                        ?>
                                <a href=<?=ABS_PATH . "profile/delete_repository?crate=" . $row["repositoryId"]?> class="btn background-transparent pixcrate-pane-button d-block rounded-0">
                                    <span class="text-center text-truncate d-block center-align align-middle pixcrate-text-dark mr-1">
                                        Delete
                                    </span>
                                    <i class="material-icons d-block align-middle pixcrate-text-dark">delete</i>
                                </a>
                        <?php
                            }
                        ?>
                    </div>
                </div>
        </div>
        <?php
            }
        ?>
    </div>
    <div class="col-12 col-sm-3 border-left"></div>
</div>