<?php

    require 'core/PHPMailer/src/Exception.php'; 
    require 'core/PHPMailer/src/PHPMailer.php'; 
    require 'core/PHPMailer/src/SMTP.php';

    class Utils
    {

        public static function generateId($length = 0)
        {
            $id = "";
            $charSet = str_split("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_");

            for ($i = 0; $i < $length; $i++) {
                $id .= $charSet[rand(0, count($charSet) - 1)];
            }
            return $id;
        }

        public static function checkSession()
        {
            if (!isset($_SESSION["id"])) {
                header("Location:" . ABS_PATH);
            }
            return new static;
        }

        public static function restrictMethods()
        {
            if (isset($_SESSION["id"]) && (CFGList::getCurrentLocation() == ABS_PATH . "login" || CFGList::getCurrentLocation() == ABS_PATH . "login/")) {
                header("Location:" . ABS_PATH . "home");
            } else if (isset($_SESSION["id"]) && (CFGList::getCurrentLocation() == ABS_PATH . "register" || CFGList::getCurrentLocation() == ABS_PATH . "register/")) {
                header("Location:" . ABS_PATH . "home");
            } else if (isset($_SESSION["id"]) && (CFGList::getCurrentLocation() == ABS_PATH . "verify" || CFGList::getCurrentLocation() == ABS_PATH . "verify/")) {
                header("Location:" . ABS_PATH . "home");
            } else if (isset($_SESSION["id"]) && (CFGList::getCurrentLocation() == ABS_PATH || CFGList::getCurrentLocation() == ABS_PATH . "/")) {
                header("Location:" . ABS_PATH . "home");
            } else if (isset($_SESSION["serveradmin"]) && $_SESSION["serveradmin"] !=1 && (CFGList::getCurrentLocation() == ABS_PATH . "management" || CFGList::getCurrentLocation() == ABS_PATH . "management/")) {
                header("Location:" . ABS_PATH . "home");
            }
        }

        public static function startSession()
        {
            if (!isset($_SESSION)) {
                session_start();
            }
            return new static;
        }

        public static function rmdir_recursive($dir) {
            foreach(scandir($dir) as $file) {
                if ('.' === $file || '..' === $file) continue;
                if (is_dir("$dir/$file")) rmdir_recursive("$dir/$file");
                else unlink("$dir/$file");
            }
            rmdir($dir);
        }

        public static function mail($to="", $subject="" , $msg) {
            $mail = new PHPMailer\PHPMailer\PHPMailer();
            $mail->isSMTP();
            $mail->SMTPAuth = true;
            $mail->SMTPSecure = "tls";
            $mail->Host = "smtp.gmail.com";
            $mail->Port = 587;
            $mail->Username = "pixcrate@gmail.com";
            $mail->Password = "";
            $mail->setFrom("noreply@pixcrate.com");
            $mail->Subject = $subject;
            $mail->Body = $msg;
            //$mail->SMTPDebug = 2;
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
            $mail->addAddress($to);

            $isSent = $mail->send();

            if(!$isSent) {
                echo "Mailer Error: " . $mail->ErrorInfo;
            }
        }

    }
?>